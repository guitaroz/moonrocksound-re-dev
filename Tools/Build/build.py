import os
import shutil
import array
import re
import string
import zipfile
import sys
import buildconfig
import glob
import stat
import traceback
import subprocess
import pipes


def verbosePrint(string):
	if buildconfig.VERBOSE_BUILD:
		print string

# begin snip -- copied from GenerateJukeboxVersionInfoFiles.py

def traverse_dir_path_to_parent(iPath, iTimes):
	assert os.path.isdir(iPath)
	path = iPath
	for n in range(iTimes):
		path = os.path.dirname(path)
	return path

# end snip

# Find LLVM distribution
LLVM_DIR = ""
try:
	LLVM_DIR = os.path.join(buildconfig.JUKEBOX_SDK_DIR, os.path.normpath("Tools/LLVM"))
	llvmFile = file(os.path.join(LLVM_DIR, os.path.normpath("Mac/Deployment/bin/llc")), 'r')
	llvmFile.close()
except:
	# This option is only intended for PH-internal purposes
	LLVM_DIR = os.path.join(buildconfig.JUKEBOX_SDK_DIR, os.path.normpath("../../LLVM/Dist"))
	if not os.path.exists(LLVM_DIR):
		LLVM_DIR = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "..", os.path.normpath("LLVM"))
verbosePrint("Using " + LLVM_DIR)



	




# Intermediate and Output directories
INTERMEDIATE_DIR = "Intermediate-llvm"
OUTPUT_DIR = "Output"
UNIVERSAL45_TMP = os.path.join(INTERMEDIATE_DIR, "Universal45")
UNIVERSAL45_OUTPUT = os.path.join(OUTPUT_DIR, "Universal45")
LOCAL45_OUTPUT = os.path.join(OUTPUT_DIR, "Local45")
GUI_DIR = "GUI"
GUI_2D_DIR = "GUI2D"
LOCAL45_GUI_DIR = os.path.join(GUI_DIR, "Output")
UNIVERSAL45_GUI_DIRS = [
	os.path.join(GUI_DIR, os.path.normpath("resources/geometries")),
	os.path.join(GUI_DIR, os.path.normpath("resources/materials")),
	os.path.join(GUI_DIR, os.path.normpath("resources/scenegraphs")),
	os.path.join(GUI_DIR, os.path.normpath("resources/scripts")),
	os.path.join(GUI_DIR, os.path.normpath("resources/textures")),
	]

	
# Paths to CRT files
CRT_FILES_PATH = os.path.normpath("Jukebox/libc/lib")


# Mac specific build parameters
SYSLIBROOT = ""
MACOSX_VERSION_MIN = "-macosx_version_min 10.5"


# Add ShimABI to source files
SHIMABISOURCE = os.path.join(buildconfig.JUKEBOX_SDK_DIR, os.path.normpath("Tools/Libs/Jukebox/ShimABI/JukeboxABI.cpp"))
buildconfig.SOURCE_FILES.append(SHIMABISOURCE)

# Parse wildcards in SOURCE_FILES
SOURCE_FILES = []
for sourceFilesEntry in buildconfig.SOURCE_FILES:
	for globFile in glob.iglob(os.path.normpath(sourceFilesEntry)):
		SOURCE_FILES.append(globFile)

# Parse os specific paths in INCLUDE_DIRS
INCLUDE_DIRS = []
for includeDirsEntry in buildconfig.INCLUDE_DIRS:
	INCLUDE_DIRS.append(os.path.normpath(includeDirsEntry))



#
# File helpers
#

def isValidFileForZipArchive(file):
    return not (file.startswith(".") or file.endswith("~"))

def makeZipArchive(zipFile, sourceDir):
    zip = zipfile.ZipFile(zipFile, mode='w', compression=zipfile.ZIP_DEFLATED, allowZip64=True)
    rootlen = len(sourceDir) + 1
    for base, dirs, files in os.walk(sourceDir):
        for file in files:
            if isValidFileForZipArchive(file):
                fn = os.path.join(base, file)
                zip.write(fn, fn[rootlen:])

def removeContentsOfDirectory(directory):
	for root, dirs, files in os.walk(directory, topdown=False):
		for file in files:
			filePath = os.path.join(root, file)
			os.chmod(filePath, stat.S_IWUSR | stat.S_IRUSR)
			os.remove(filePath)
		
		for dir in dirs:
			dirPath = os.path.join(root, dir)
			os.rmdir(dirPath)

def chmodAndRmtree(directory):
	for root, dirs, files in os.walk(directory):
		for file in files:
			filePath = os.path.join(root, file)
			os.chmod(filePath, stat.S_IWUSR | stat.S_IRUSR)
	shutil.rmtree(directory, ignore_errors=True)

def copyFiles(srcDir, dstDir):
	for root, dirs, files in os.walk(srcDir):
		for file in files:
			srcPath = os.path.join(root, file)
			dstPath = os.path.join(dstDir, file)
			shutil.copy2(srcPath, dstPath)

def existsAnyFiles(dir):
	try:
		return len(os.listdir(dir)) > 0
	except OSError:
		return False
	
			
#
# String helpers
#

def replaceEndOfString(iString, iOldEnd, iNewEnd):
	parts = iString.rsplit(iOldEnd, 1)
	assert len(parts) == 2, "replaceEndOfString(): iOldEnd not found"
	return "%s%s" % (parts[0], iNewEnd)

def objNameFromSourceName(fileName):
	if isEndOfStringMatched(fileName, ".c"):
		return replaceEndOfString(fileName, ".c", ".o.bc")
	else:
		return replaceEndOfString(fileName, ".cpp", ".o.bc")

def removeOneFilenameExtension(ext, name):
	return name.rstrip(ext)

def isEndOfStringMatched(iString, iEnd):
	matched = False
	l1 = len(iString)
	l2 = len(iEnd)
	if l2 <= l1:
		matched = (iString[l1-l2:] == iEnd)
	return matched


def getWinPlatformString(platform):
	if platform == 32:
		return "Win32"
	elif platform == 64:
		return "x64"
	return ""



# Return values:
#   -1 = error
#   0 = all
#   32 = 32 bit
#   64 = 64 bit
def parsePlatformArgument(argument):
	if str(argument) == "all":
		return 0
	if str(argument) == "i386" or str(argument) == "32" or str(argument) == "Win32":
		return 32
	elif str(argument) == "64" or str(argument) == "x86_64" or str(argument) == "x64":
		return 64
	else:
		return -1




# Returns file name at the end of a path
def removePath(fileName):
	pathDivider = os.path.normpath("/")
	if pathDivider == "\\":
		pathDivider = "\\\\"
	fileNameParts = re.split(pathDivider, fileName)
	fileNameWithoutPath = fileNameParts.pop()
	return fileNameWithoutPath




#
# Variable and constants helpers
#

def getLLVMPath(osType):
	llvmPath = ""
	if osType == "Mac":
		llvmPath = os.path.join(LLVM_DIR, "Mac", "Deployment", "bin")
	elif osType == "Win":
		llvmPath = os.path.join(LLVM_DIR, "Win32", "Deployment")
	return llvmPath

#
# Gets the full path to a CRT LL object file based on "component" partial file name.
# For example, specifying "crt" and "Deployment" will prefix the full path and suffix with
# a ".o".
#
def getCrtFile(platform, configuration, component):
	target = ""
	if platform == 32:
		target = "phdsp32"
	else:
		target = "phdsp64"

	suffix = ""	
	if configuration == "Deployment":
		suffix = ".o.bc"
	else:
		suffix = "d.o.bc"
		
	object = component + suffix
	return os.path.join(LLVM_DIR, os.path.join(CRT_FILES_PATH, target, object))


def getLibFolderPath(libname, platform):
	assert libname == "libc" or libname == "libcxx" or libname == "compiler-rt", "getLibFolderPath(): libname must be 'libc', 'libcxx' or 'compiler-rt'"

	if libname == "compiler-rt":
		return os.path.join(LLVM_DIR, "Jukebox", "%s", "lib") % (libname)

	targetCPU = "phdsp32"
	if platform == 64:
		targetCPU = "phdsp64"

	return os.path.join(LLVM_DIR, "Jukebox", "%s", "lib", "%s") % (libname, targetCPU)	

def getClangLibs(platform, configuration):
	leadingPathC = getLibFolderPath("libc", platform)
	leadingPathCPP = getLibFolderPath("libcxx", platform)

	# Order is important! libcpp before libc!
	returnList = [
		os.path.join(leadingPathCPP, "libcpp.bc"),
		os.path.join(leadingPathC, "libc.bc")
		]

	return returnList

def getCompilerRTLib():
	folderPath = getLibFolderPath("compiler-rt", 32)
	return os.path.join(folderPath, "compiler-rt.bc")


def getClangNonTargetOptions(configuration):
	args = ["-DJUKEBOX_SDK=1"]
	if configuration == "Debugging":
		args += ["-DDEBUG=1"]
	elif configuration == "Testing":
		args += ["-DDEBUG=1"]
	elif configuration == "Deployment":
		args += ["-DNDEBUG=1"]
	
	for includeDir in INCLUDE_DIRS:
		args.append("-I" + includeDir)
	
	args += [
		"-I" + os.path.join(buildconfig.JUKEBOX_SDK_DIR, "API"),
		"-I" + os.path.join(LLVM_DIR, os.path.normpath("Jukebox/libc/include")),
		"-I" + os.path.join(LLVM_DIR, os.path.normpath("Jukebox/libcxx/include")),
		]
	
	return string.join(args, " ")

def getClangTargetOptions(osType, platform, configuration):

	args = []

	if osType == "Mac":
		args += ["-ccdsp", "-fno-builtin", "-fcxx-exceptions", "-ffreestanding"]
	elif osType == "Win":
		args += ["-ccdsp", "-fno-builtin", "-fdiagnostics-format msvc", "-fcxx-exceptions", "-ffreestanding"]

	if platform == 32:
		args += ["-target-cpu phdsp32"]
	elif platform == 64:
		args += ["-target-cpu phdsp64"]

	if configuration == "Deployment":
		args += ["-O2"]

	return string.join(args, " ")


def getClangAnalysisArch(platform):
	returnString = ""
	if platform == 32:
		returnString = "i386"
	elif platform == 64:
		returnString = "x86_64"
	return returnString


def getLLCFlags(osType):
	if osType == "Mac":
		# -static-init-in-data is needed to make initializer tables accessible for the JB runtime
		return "-relocation-model=pic -filetype=obj -static-init-in-data"
	elif osType == "Win":
		return "-filetype=obj"
	return ""


	
def getLLCPlatform(platform, osType):
	if osType == "Mac":
		if platform == 32:
			return "-mtriple=i686-apple-darwin9.0.0 -march=x86 -mcpu=yonah -mattr=+sse3"
		elif platform == 64:
			return "-mtriple=x86_64-apple-darwin9.0.0 -march=x86-64 -mcpu=x86-64 -mattr=+sse3"
	elif osType == "Win":
		# the -jbcrt platform variant turns on JB initializer table behavior
		if platform == 32:
			return "-mtriple=i686-pc-win32-jbcrt -march=x86 -mcpu=pentium-m -mattr=+sse2"
		elif platform == 64:
			return "-mtriple=x86_64-pc-win32-jbcrt -march=x86-64 -mcpu=x86-64 -mattr=+sse3"
	return ""



def getOptParameters(platform, configuration, osType):
	if configuration == "Deployment":
		returnString = "-disable-simplify-libcalls -strip -insert-timeout-checks -ph-timeout-ch=1000 -ph-timeout-cll=100"
	else:
		returnString = "-disable-simplify-libcalls -insert-timeout-checks -ph-timeout-ch=1000 -ph-timeout-cll=100"

	if platform == 64 and osType == "Win":
		# -strip-debug is needed, since clang produces 32 bit debug symbols, that link.exe does not handle (at least not with vs2008)
		returnString += " -strip-debug"
	return returnString


def getRackExtWrapperLib(platform, configuration, osType):
	reWrapperLib = ""
	
	if osType == "Mac":
		reWrapperLib = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "Tools", "Libs", "RackExtensionWrapper", "Mac", configuration, "libRackExtWrapperLib.a")
		# Only for internal PH use
		if not os.path.exists(reWrapperLib):
			reWrapperLib = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "../../Output", "Mac", "RackExtWrapperLib", configuration, "libRackExtWrapperLib.a")
	else:
		platformString = getWinPlatformString(platform)
		reWrapperLib = os.path.join(buildconfig.JUKEBOX_SDK_DIR, os.path.join("Tools", "Libs", "RackExtensionWrapper", platformString, configuration, "RackExtWrapperLib.lib"))
		# Only for internal PH use
		if not os.path.exists(reWrapperLib):
			reWrapperLib = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "../../Output", "RackExtWrapperLib", platformString, configuration, "RackExtWrapperLib.lib")

	verbosePrint("Using " + reWrapperLib)
	return reWrapperLib


def getRackExtWrapperDLLExports(platform, configuration):
	fileName = ""
	platformString = getWinPlatformString(platform)
	fileName = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "Tools", "Libs", "RackExtensionWrapper", platformString, "WinDLLExports.txt")
	# Only for internal PH use
	if not os.path.exists(fileName):
		fileName = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "..", "RackExtWrapperLib", "WinDLLExports.txt")
	return fileName


def runCommand(commandLine):
	verbosePrint(commandLine)
	returnValue = os.system(commandLine)
	if returnValue != 0:
		raise Exception()


#
# Compiler helpers
#

def llc(inputFile, outputFile, optimizationFlags, platform, configuration, osType):
	llcCommand = os.path.join(getLLVMPath(osType), "llc")
	commandLine = string.join([llcCommand, optimizationFlags, getLLCPlatform(platform, osType), getLLCFlags(osType), inputFile, "-o", outputFile])
	runCommand(commandLine)



def clang(inputFile, outputFile, platform, configuration, osType, otherCompilerFlags):
	clangCommand = os.path.join(getLLVMPath(osType), "clang")

	if buildconfig.STATIC_ANALYSIS:
		# JE: -fno-ms-compatibility makes clang predefine __UINT<n>__ needed by clang stdint.h
		# -nostdlibinc is always on in phdsp target (when using -ccdsp)
		# JE: using sys=darwin in target triple, because clang is less stable on Windows
		# (and we should never infer arch from the compiling system)
		clangArgs = [
			"--analyze",
			"-target %s-darwin" % (getClangAnalysisArch(platform) ),
			"-O0",
			"-g0",
			"-fmsc-version=0",
			"-fno-ms-compatibility",
			"-D__STDC__",
			getClangNonTargetOptions(configuration),
			"-nostdlibinc",
			otherCompilerFlags,
			"-D__phdsp__"]

		if platform == 64:
			clangArgs.append("-D__phdsp64__")
		else:
			clangArgs.append("-D__phdsp32__")

		# JE: std= to match the apparent settings when using -ccdsp
		if isEndOfStringMatched(inputFile, ".c"):
			clangArgs.append("-std=gnu99")
		else:
			clangArgs.append("-std=gnu++11")

		commandLineAnalyze = string.join(
			[clangCommand] +
			clangArgs +
			[inputFile], " ")

		runCommand(commandLineAnalyze)

	clangBuildParameters = string.join([
		getClangTargetOptions(osType, platform, configuration),
		getClangNonTargetOptions(configuration),
		otherCompilerFlags, "-Wuninitialized -Werror=uninitialized"])	
	commandLineBuild = string.join([clangCommand, clangBuildParameters, inputFile, "-o", outputFile])
	runCommand(commandLineBuild)



def optAddInstrumentation(inputFile, outputFile, platform, configuration, osType):
	optCommand = os.path.join(getLLVMPath(osType), "opt")
	commandLine = string.join([optCommand, getOptParameters(platform, configuration, osType), inputFile, "-o=" + outputFile])
	runCommand(commandLine)

def optCheckDisableGlobals(inputFile, osType):
	optCommand = os.path.join(getLLVMPath(osType), "opt")
	commandLine = string.join([optCommand, "-ph-disable-globals", inputFile, "-o=" + "temp.txt"])
	runCommand(commandLine)



def llvm_link(inputFiles, outputFile, inputLibs, platform, configuration, osType):
	llvmLinkCommand = os.path.join(getLLVMPath(osType), "llvm-link")
	files = string.join(inputFiles)
	libs = string.join(inputLibs)
	commandLine = string.join([llvmLinkCommand, "-o=" + outputFile, files, libs])
	runCommand(commandLine)



def ld(inputFiles, outputFile, platform, configuration, symbols):
	# Sniff to make sure that ld exists
	returnValue = os.system("ld -v &> /dev/null")
	if returnValue != 0:
		print "error: The ld command is missing from the system. Please install the Xcode command line tools, available at http://developer.apple.com"
		raise Exception()

	exports = "-exported_symbol _JukeboxExport_InitializeDLL -exported_symbol _JukeboxExport_CreateNativeObject -exported_symbol _JukeboxExport_RenderRealtime"

	commandLine = string.join([
		"ld", 
		"-dead_strip",
		"-dylib", 
		SYSLIBROOT, 
		MACOSX_VERSION_MIN, 
		"-keep_private_externs", 
		"-o", 
		outputFile, 
		exports, 
		string.join(inputFiles), 
		getRackExtWrapperLib(platform, configuration, "Mac"), 
		"/usr/lib/dylib1.o -lc"
		])

	if symbols == "strip":
		commandLine = string.join([commandLine, "-x"])

	runCommand(commandLine)




#
# Build stage helpers
#

def addInstrumentationAndCompileToObj(inputFile, platform, configuration, osType, optFlags):
	# JE: legacy from SDK 1 - original file ends with .ll when building IR
	inputFileBC = inputFile
	if isEndOfStringMatched(inputFile, ".ll"):
		inputFileBC = replaceEndOfString(inputFile, ".ll", ".bc")
	instrumentedInputFile = replaceEndOfString(inputFileBC, ".bc", "Inst.bc")
	optAddInstrumentation(inputFile, instrumentedInputFile, platform, configuration, osType)
	convertedInstrInputFile = replaceEndOfString(instrumentedInputFile, ".bc", ".obj")
	llc(instrumentedInputFile, convertedInstrInputFile, optFlags, platform, configuration, osType)
	return convertedInstrInputFile




#
# Build stages
#

def build45IntermediateFormat(rackExtensionName, intermediateDir, platform, configuration, osType, otherCompilerFlags):
	# Build list of intermediate .o.bc file names from .cpp file names
	bcFiles = []
	for sourceFile in SOURCE_FILES:
		bcFile = objNameFromSourceName(sourceFile)
		bcFile = removePath(bcFile)
		bcFiles.append(os.path.join(intermediateDir, bcFile))

	# Build .cpp files into intermediate .o.bc files
	for dspFile in SOURCE_FILES:
		# If VERBOSE_BUILD is True, we will get much more output printed anyway. This is only for low verbosity builds.
		if not buildconfig.VERBOSE_BUILD and not dspFile==SHIMABISOURCE:	
			print "Building " + dspFile

		outputFile = objNameFromSourceName(removePath(dspFile));
		outputFile = os.path.join(intermediateDir, outputFile)
		clang(dspFile, outputFile, platform, configuration, osType, otherCompilerFlags)
		optCheckDisableGlobals(outputFile, osType)

	# Link intermediate .o.bc files into a single .bc file
	outputFile = os.path.join(intermediateDir, rackExtensionName + "_static_library.bc")
	clangLibs = getClangLibs(platform, configuration)
	llvm_link(bcFiles, outputFile, clangLibs, platform, configuration, osType)
	return outputFile



# Build a platform specific dylib
def build45Dylib(rackExtensionName, inputFile, outputFile, intermediateDir, platform, configuration, symbols):
	optFlags = "-O0"
	if configuration == "Deployment":
		optFlags = "-O2"

	# JE: 32 bit Mac built arithmetic support for 64 bit types
	if platform == 32:
		# JE: legacy from SDK 1 - original file ends with .ll when building IR
		inputFileBC = inputFile
		if isEndOfStringMatched(inputFile, ".ll"):
			inputFileBC = replaceEndOfString(inputFile, ".ll", ".bc")
		prelinkedInputFile = replaceEndOfString(inputFileBC, ".bc", "-with-compiler-rt.bc")
		llvm_link([inputFile, getCompilerRTLib()], prelinkedInputFile, [], platform, configuration, "Mac")
		inputFile = prelinkedInputFile

	# Convert CRT bitcode (.o.bc) files to native platform object files
	filesToLink = []
	for crtFile in ('crtbegin', 'crt', 'crtend'):
		crtFile = getCrtFile(platform, configuration, crtFile)
		convertedCrtFile = os.path.join(intermediateDir, removeOneFilenameExtension(".bc", removePath(crtFile)))
		filesToLink.append(convertedCrtFile)
		llc(crtFile, convertedCrtFile, optFlags, platform, configuration, "Mac")

	# Add Propellerhead instrumentation
	instrumentedFile = addInstrumentationAndCompileToObj(inputFile, platform, configuration, "Mac", optFlags)
	# Insert after crt
	filesToLink.insert(2, instrumentedFile)
	
	# Link dylib with MacOS tools
	ld(filesToLink, outputFile, platform, configuration, symbols)



# Build a universal binary version of a dylib
def buildUniversalBinaryDylib(rackExtensionName, inputFile32, inputFile64, intermediateDir):
	dylibName = os.path.join(intermediateDir, rackExtensionName + ".dylib")
	commandLine = string.join(["lipo", inputFile32, inputFile64, "-create -output", dylibName])
	runCommand(commandLine)
	return dylibName

# Build the pico libc. Required to test that we don't depend on libc on Windows and to wrap float functions on 32 bit Win.
def buildPicoLibc(intermediateDir, platform, configuration, optFlags):
	dummyLibcFile = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "Tools", "Libs", "VisualStudio", "picolibc.c")
	dummyLibcLLFile = objNameFromSourceName(removePath(dummyLibcFile))
	dummyLibcLLFile = os.path.join(intermediateDir, dummyLibcLLFile)

	dummyOutputObjFile = os.path.join(intermediateDir, "dummylibc.obj")
	clang(dummyLibcFile, dummyLibcLLFile, platform, configuration, "Win", "-D _INTRINSICSTEST")
	llc(dummyLibcLLFile, dummyOutputObjFile, optFlags, platform, configuration, "Win")
	
	bitnessDefinition = "_32BIT"
	if platform == 64:
		bitnessDefinition = "_64BIT"
	wrapperOutputObjFile = os.path.join(intermediateDir, "wrapperlibc.obj")
	clang(dummyLibcFile, dummyLibcLLFile, platform, configuration, "Win", "-D _WRAPPER -D " + bitnessDefinition)
	llc(dummyLibcLLFile, wrapperOutputObjFile, optFlags, platform, configuration, "Win")

	return [dummyOutputObjFile, wrapperOutputObjFile]
	
	
# Build a platform specific dll
def build45DLL(rackExtensionName, inputFile, outputFile, intermediateDir, platform, configuration):
	optFlags = "-O0"
	if configuration == "Deployment":
		optFlags = "-O2"

	# Link the intermediate .bc files and libc using llvm-link
	crtFile = getCrtFile(platform, configuration, 'crt')
	convertedCrtFile = os.path.join(intermediateDir, removeOneFilenameExtension(".bc", removePath(crtFile)))
	llc(crtFile, convertedCrtFile, optFlags, platform, configuration, "Win")

	# Add Propellerhead instrumentation
	instrumentedFile = addInstrumentationAndCompileToObj(
		inputFile, platform, configuration, "Win", optFlags)

	platformString = getWinPlatformString(platform)
	libPath = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "Tools", "Libs", "VisualStudio", platformString, configuration)
	glueLib = 'CrtGlueLib.lib'

	# PH internal use
	if not os.path.exists(os.path.join(libPath, glueLib)):
		libPath = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "../../Output", "CrtGlueLib", platformString, configuration)
		

	# Link with empty libc except for some dummy math functions using Visual Studio tools,
	# just to test that we don't depend on anything else.
	[dummyLibcObjFile, wrapperLibcObjFile] = buildPicoLibc(
		intermediateDir, platform, configuration, optFlags)

	commonFilesToLink = [
		instrumentedFile,
		convertedCrtFile,
		getRackExtWrapperLib(platform, configuration, "Win"),
		glueLib]
	
	commonLinkOptions = [
		"/NOLOGO",
		"/DLL",
		"/NOENTRY",
		"/LIBPATH:" + libPath,
		"@" + getRackExtWrapperDLLExports(platform, configuration)]
	
	commonLinkOptions.append("/INCREMENTAL:NO")
	
	if configuration == "Deployment":
		commonLinkOptions.append("/OPT:REF")
		commonLinkOptions.append("/OPT:ICF")
	else:
		commonLinkOptions.append("/DEBUG")
		commonLinkOptions.append("/PDB:NONE")
	
	commandLine = string.join(
		["link"] +
		commonLinkOptions + [
			"/NODEFAULTLIB",
			"/OUT:dependencytest.dll"] +
		commonFilesToLink + [dummyLibcObjFile], " ")
	runCommand(commandLine)
	
	# If the above worked and didn't raise an exception, link DLL with default libraries instead of dummy libc
	# to produce the final result.
	commandLine = string.join(
		["link"] +
		commonLinkOptions + [
			"/NODEFAULTLIB:MSVCRT",
			"/NODEFAULTLIB:MSVCRTD",
			"/DEFAULTLIB:LIBCMT",
			"/NODEFAULTLIB:LIBCMTD",
			"/NODEFAULTLIB:LIBCPMT",
			"/NODEFAULTLIB:LIBCPMTD",
			"/OUT:" + outputFile] +
		commonFilesToLink + [wrapperLibcObjFile], " ")
	runCommand(commandLine)


#
# Build targets
#
def getLLVMBuiltLibraryName(rackExtensionName, osType, platform, configuration):
	intermediateDir = os.path.join(INTERMEDIATE_DIR, configuration, str(platform))
	if osType == "Mac":
		return os.path.join(intermediateDir, rackExtensionName + ".dylib")
	elif osType == "Win":
		return os.path.join(intermediateDir, rackExtensionName + str(platform) + ".dll")
	return None

# Make version.txt for the 45 build by copying from SDK, then appending LLVM and clang info
def makeVersionTextFile(iSDKDirPath, iDestDirPath, iOSType):
	versionFilePath = os.path.join(iSDKDirPath, os.path.normpath("version.txt"))
	
	if os.path.exists(versionFilePath):
		shutil.copy2(versionFilePath, iDestDirPath)
	else:
		# internal Dev-build, use local file
		jukeboxDevPath = traverse_dir_path_to_parent(os.path.abspath(iSDKDirPath), 2)
		versionFilePath = os.path.join(jukeboxDevPath, "JukeboxSDK-version.txt")
		destPath = os.path.join(iDestDirPath, "version.txt")
		assert os.path.isfile(versionFilePath), "Missing file '%s'" % versionFilePath
		shutil.copy2(versionFilePath, destPath)
	
	# JE: append LLVM and clang version info
	
	versionFilePath = os.path.join(iDestDirPath, os.path.normpath("version.txt"))
	versionAppendFile = open(versionFilePath, 'ab')
	versionAppendFile.write('\n')
	
	versionAppendFile.write('llc: ')
	llcCommand = os.path.join(getLLVMPath(iOSType), "llc")
	llcVersion = subprocess.Popen([llcCommand, '--version'], stdout=subprocess.PIPE).communicate()[0]
	# JE: convert platform endlines to unix
	versionAppendFile.write(str("\n").join(llcVersion.splitlines() ) )
	versionAppendFile.write("\n")
	versionAppendFile.write('clang: ')
	clangCommand = os.path.join(getLLVMPath(iOSType), "clang")
	clangVersion = subprocess.Popen([clangCommand, '--version'], stdout=subprocess.PIPE).communicate()[0]
	# JE: convert platform endlines to unix
	versionAppendFile.write(str("\n").join(clangVersion.splitlines() ) )
	
	versionAppendFile.close()


def local45(rackExtensionName, platform, configuration, osType, otherCompilerFlags, productID):
	# Remove old intermediate directory
	chmodAndRmtree(INTERMEDIATE_DIR)

	# Create intermediate directories for target arch
	intermediateDir = os.path.join(INTERMEDIATE_DIR, configuration, str(platform))
	os.makedirs(intermediateDir)

	# Build the intermediate format files (Build cpp files and link together to one bitcode file)
	intermediate45File = build45IntermediateFormat(rackExtensionName, intermediateDir, platform, configuration, osType, otherCompilerFlags)

	builtLibraryName = getLLVMBuiltLibraryName(rackExtensionName, osType, platform, configuration)
	if osType == "Mac":
		# Build platform specific dylib
		build45Dylib(rackExtensionName, intermediate45File, builtLibraryName, intermediateDir, platform, configuration, "keep")
	elif osType == "Win":
		# Build platform specific dll
		build45DLL(rackExtensionName, intermediate45File, builtLibraryName, intermediateDir, platform, configuration)
	
	# Remove contents (only) of the old output directory (Don't remove the entire directory on Windows with Reason running)
	installDir = getInstallDir(rackExtensionName)
	removeContentsOfDirectory(installDir)

	# Install built local45
	copyResources(osType, installDir)
	copyLibrary(builtLibraryName, rackExtensionName, osType, platform, installDir, productID)


def local45All(rackExtensionName, configuration, osType, otherCompilerFlags, productID):
	# Remove old intermediate directory
	chmodAndRmtree(INTERMEDIATE_DIR)

	# Create intermediate directories for both 32 and 64 bit
	intermediateDir32 = os.path.join(INTERMEDIATE_DIR, configuration, "32")
	os.makedirs(intermediateDir32)
	intermediateDir64 = os.path.join(INTERMEDIATE_DIR, configuration, "64")
	os.makedirs(intermediateDir64)

	# Build the intermediate format files (Build cpp files and link together to one bitcode file)
	intermediate45File32 = build45IntermediateFormat(rackExtensionName, intermediateDir32, 32, configuration, osType, otherCompilerFlags)
	intermediate45File64 = build45IntermediateFormat(rackExtensionName, intermediateDir64, 64, configuration, osType, otherCompilerFlags)

	if osType == "Mac":
		# Build the 32 and 64 bit versions of the dylib
		dylib32Name = os.path.join(intermediateDir32, rackExtensionName + "32.dylib")
		build45Dylib(rackExtensionName, intermediate45File32, dylib32Name, intermediateDir32, 32, configuration, "keep")

		dylib64Name = os.path.join(intermediateDir64, rackExtensionName + "64.dylib")
		build45Dylib(rackExtensionName, intermediate45File64, dylib64Name, intermediateDir64, 64, configuration, "keep")

		# Build the universal binary version of the dylib
		# FL: Put universal lib in 64 dir
		unversalDylib = buildUniversalBinaryDylib(rackExtensionName, dylib32Name, dylib64Name, intermediateDir64)
	elif osType == "Win":
		dllName32 = os.path.join(outputDir, rackExtensionName + str(platform) + "32.dll")
		build45DLL(rackExtensionName, intermediate45File32, dllName32, intermediateDir32, 32, configuration)
		dllName64 = os.path.join(outputDir, rackExtensionName + str(platform) + "64.dll")
		build45DLL(rackExtensionName, intermediate45File64, dllName64, intermediateDir64, 64, configuration)

	# Remove the old output directory
	installDir = getInstallDir(rackExtensionName)
	chmodAndRmtree(installDir)

	# Install built local45
	copyResources(osType, installDir)
	if osType == "Win":
		copyLibrary(dllName32, rackExtensionName, osType, 32, installDir, productID)
		copyLibrary(dllName64, rackExtensionName, osType, 64, installDir, productID)
	else:
		copyLibrary(unversalDylib, rackExtensionName, osType, 0, installDir, productID)


def universal45Copy3DGUI():
	for guiDir in UNIVERSAL45_GUI_DIRS:
		if not os.path.exists(guiDir):
			raise Exception("No HD GUI folder named '" + guiDir + "'")
		if not existsAnyFiles(guiDir):
			raise Exception("HD GUI folder '" + guiDir + "' is empty")
		os.makedirs(os.path.join(UNIVERSAL45_TMP, guiDir))
		copyFiles(guiDir, os.path.join(UNIVERSAL45_TMP, guiDir))
	for deviceFile in glob.iglob(os.path.join(GUI_DIR, "*.device")):
		shutil.copy2(deviceFile, os.path.join(UNIVERSAL45_TMP, GUI_DIR))

def universal45Copy2DGUI():
	os.makedirs(os.path.join(UNIVERSAL45_TMP, "GUI2D"))
	for file in glob.iglob(os.path.join(GUI_2D_DIR, "*.png")):
		shutil.copy2(file, os.path.join(UNIVERSAL45_TMP, GUI_2D_DIR))
	for file in glob.iglob(os.path.join(GUI_2D_DIR, "*.lua")):
		shutil.copy2(file, os.path.join(UNIVERSAL45_TMP, GUI_2D_DIR))



def universal45(rackExtensionName, osType, otherCompilerFlags):
	# Remove old intermediate directory
	chmodAndRmtree(INTERMEDIATE_DIR)
	os.makedirs(INTERMEDIATE_DIR)

	# Testing
	# Create intermediate directories for both 32 and 64 bit
	intermediateDir32 = os.path.join(INTERMEDIATE_DIR, "Testing", "32")
	os.makedirs(intermediateDir32)
	intermediateDir64 = os.path.join(INTERMEDIATE_DIR, "Testing", "64")
	os.makedirs(intermediateDir64)
	
	# Build the intermediate format files (Build cpp files and link together to one bitcode file)
	intermediateTesting45File32 = build45IntermediateFormat(rackExtensionName, intermediateDir32, 32, "Testing", osType, otherCompilerFlags)
	intermediateTesting45File64 = build45IntermediateFormat(rackExtensionName, intermediateDir64, 64, "Testing", osType, otherCompilerFlags)
	
	# Deployment
	# Create intermediate directories for both 32 and 64 bit
	intermediateDir32 = os.path.join(INTERMEDIATE_DIR, "Deployment", "32")
	os.makedirs(intermediateDir32)
	intermediateDir64 = os.path.join(INTERMEDIATE_DIR, "Deployment", "64")
	os.makedirs(intermediateDir64)
	
	# Build the intermediate format files (Build cpp files and link together to one bitcode file)
	intermediateDeployment45File32 = build45IntermediateFormat(rackExtensionName, intermediateDir32, 32, "Deployment", osType, otherCompilerFlags)
	intermediateDeployment45File64 = build45IntermediateFormat(rackExtensionName, intermediateDir64, 64, "Deployment", osType, otherCompilerFlags)

	# Copy resources and built files to the output directory
	shutil.copytree("Resources", UNIVERSAL45_TMP)
	for luaFile in glob.iglob("*.lua"):
		shutil.copy(luaFile, UNIVERSAL45_TMP)

	print "Copying GUI resources"
	guiType = "none"
	if os.path.exists(UNIVERSAL45_GUI_DIRS[0]):
		guiType = "3D"
		universal45Copy3DGUI()
	if os.path.exists(GUI_2D_DIR):
		if guiType != "none":
			raise Exception("Both 2D and 3D GUIs found")
		guiType = "2D"
		universal45Copy2DGUI()
	if guiType == "none":
		raise Exception("No GUI folder found")
		
	chipBinariesDirTesting = os.path.join(UNIVERSAL45_TMP, "chip_binaries", "Testing")
	chipBinariesDirDeployment = os.path.join(UNIVERSAL45_TMP, "chip_binaries", "Deployment")
	os.makedirs(chipBinariesDirTesting)
	os.makedirs(chipBinariesDirDeployment)

	# JE: legacy from SDK 1 - original file ends with .ll when building IR
	shutil.copy2(intermediateTesting45File32, os.path.join(chipBinariesDirTesting, rackExtensionName + "32.ll"))
	shutil.copy2(intermediateTesting45File64, os.path.join(chipBinariesDirTesting, rackExtensionName + "64.ll"))
	shutil.copy2(intermediateDeployment45File32, os.path.join(chipBinariesDirDeployment, rackExtensionName + "32.ll"))
	shutil.copy2(intermediateDeployment45File64, os.path.join(chipBinariesDirDeployment, rackExtensionName + "64.ll"))
	
	makeVersionTextFile(buildconfig.JUKEBOX_SDK_DIR, UNIVERSAL45_TMP, osType)
	
	try:
		os.makedirs(UNIVERSAL45_OUTPUT)
	except:
		print UNIVERSAL45_OUTPUT + " already exists"

	print "Creating archive"
	u45Archive = os.path.join(UNIVERSAL45_OUTPUT, rackExtensionName + ".u45")
	makeZipArchive(u45Archive, UNIVERSAL45_TMP)



def optimized45Libs(rackExtensionName, inputDir, outputDir, platform, configuration, osType):
	# Create intermediate directory
	intermediateDir = os.path.join(INTERMEDIATE_DIR, "Optimized45", configuration)
	chmodAndRmtree(intermediateDir)
	
	try:
		os.makedirs(intermediateDir)
	except:
		print intermediateDir + " already exists"
	
	# JE: legacy from SDK 1 - original file ends with .ll when building IR
	inputFile = os.path.join(inputDir, "chip_binaries", configuration, rackExtensionName + str(platform) + ".ll")
	
	if osType == "Mac":
		# Build the dylib
		dylibName = os.path.join(outputDir, rackExtensionName + ".dylib")
		build45Dylib(rackExtensionName, inputFile, dylibName, intermediateDir, platform, configuration, "strip")
	elif osType == "Win":
		dllName = os.path.join(outputDir, rackExtensionName + str(platform) + ".dll")
		build45DLL(rackExtensionName, inputFile, dllName, intermediateDir, platform, configuration)



def optimized45LibsAll(rackExtensionName, inputDir, outputDir, configuration, osType):
	# Create intermediate directory
	intermediateDir = os.path.join(INTERMEDIATE_DIR, "Optimized45", configuration)
	chmodAndRmtree(intermediateDir)
	
	try:
		os.makedirs(intermediateDir)
	except:
		print intermediateDir + " already exists"
	
	# JE: legacy from SDK 1 - original file ends with .ll when building IR
	inputFile32 = os.path.join(inputDir, "chip_binaries", configuration, rackExtensionName + "32.ll")
	inputFile64 = os.path.join(inputDir, "chip_binaries", configuration, rackExtensionName + "64.ll")

	if osType == "Mac":
		# Build the 32 and 64 bit versions of the dylib
		dylib32Name = os.path.join(intermediateDir, rackExtensionName + "32.dylib")
		build45Dylib(rackExtensionName, inputFile32, dylib32Name, intermediateDir, 32, configuration, "strip")

		dylib64Name = os.path.join(intermediateDir, rackExtensionName + "64.dylib")
		build45Dylib(rackExtensionName, inputFile64, dylib64Name, intermediateDir, 64, configuration, "strip")

		# Build the universal binary version of the dylib
		unversalDylib = buildUniversalBinaryDylib(rackExtensionName, dylib32Name, dylib64Name, outputDir)
	elif osType == "Win":
		print r"Build 'all' architectures does not work on windows currently"
		exit(1)





def smartcopy(sourcePath, destPath):
	for root, dirs, files in os.walk(sourcePath):
		dest = destPath + root.replace(sourcePath, '')
		if not os.path.isdir(dest):
			os.mkdir(dest)

		for f in files:
			oldLoc = os.path.join(root, f)
			newLoc = os.path.join(dest, f)
			if os.path.isfile(newLoc):
				if os.stat(oldLoc).st_mtime - os.stat(newLoc).st_mtime > 1:
					verbosePrint("Copying newer file: " + oldLoc)
					shutil.copy(oldLoc, newLoc)
				else:
					verbosePrint("NOT copying older or same file: " + oldLoc)
			else:
				verbosePrint("Copying file, did not exist: " + oldLoc)
				shutil.copy(oldLoc, newLoc)


def copyResources(osType, installDir):
	verbosePrint("Copying resources to installDir: " + installDir)
	for root, dirs, files in os.walk(installDir):
		for file in files:
			filePath = os.path.join(root, file)
			# make sure that the lua files being copied below will not cause errors if they already exist.
			os.chmod(filePath, stat.S_IWRITE | stat.S_IREAD)

	# Copy resources to the install directory
	#shutil.copytree("Resources", installDir)
	smartcopy("Resources", installDir)

	# Copy built HD gui resources
	if not os.path.exists(LOCAL45_GUI_DIR):
		print "WARNING: No HD GUI folder called '" + LOCAL45_GUI_DIR + "'"
	if not existsAnyFiles(LOCAL45_GUI_DIR):
		print "WARNING: HD GUI folder '" + LOCAL45_GUI_DIR + "' is empty"
	smartcopy(LOCAL45_GUI_DIR, installDir)

	# Copy lua files
	for luaFile in glob.iglob("*.lua"):
		shutil.copy(luaFile, installDir)
	makeVersionTextFile(buildconfig.JUKEBOX_SDK_DIR, installDir, osType)



def macSetLibraryID(libraryPath, identifier):
	# JP: By default, the dynamic library ID is set by the linker to be the install path + file name at link time
	# JP: When we build using Xcode, the file name is always "RELibrary.dylib"
	# JP: But the library id:s must be unique for each RE!
	commandLine = string.join([
		"install_name_tool",
		"-id",
		pipes.quote(identifier),
		pipes.quote(libraryPath)])
	runCommand(commandLine)

def copyLibrary(builtLibraryPath, rackExtensionName, osType, platform, installDir, productID):
	if osType == "Win":
		installLibraryName = rackExtensionName + str(platform) + ".dll"
	if osType == "Mac":
		installLibraryName = rackExtensionName + ".dylib"

	targetPath = os.path.join(installDir, installLibraryName)

	shutil.copy(builtLibraryPath, targetPath)
	
	winCopyPDB = (osType == "Win")
	
	if winCopyPDB:
		pdbSourcePath = replaceEndOfString(builtLibraryPath, ".dll", ".pdb")
		pdbTargetPath = replaceEndOfString(targetPath, ".dll", ".pdb")
		if os.path.isfile(pdbSourcePath):
			print "build.py: Installing .pdb"
			shutil.copy(pdbSourcePath, pdbTargetPath)
		else:
			print "build.py: No .pdb available"
	
	if osType == "Mac":
		macSetLibraryID(targetPath, productID)




def getInstallDir(rackExtensionName):
	if os.name == "nt":
		import ctypes.wintypes
		buf = ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
		CSIDL_APPDATA = 26
		SHGFP_TYPE_CURRENT= 0   # Want current, not default value
		ctypes.windll.shell32.SHGetFolderPathW(0, CSIDL_APPDATA, 0, SHGFP_TYPE_CURRENT, buf)
		appdata = buf.value
	else:
		import Carbon.File, Carbon.Folder, Carbon.Folders
		import MacOS
		fsref = Carbon.Folder.FSFindFolder(Carbon.Folders.kUserDomain, Carbon.Folders.kApplicationSupportFolderType, False)
		appdata = Carbon.File.pathname(fsref)
	
	installDir = os.path.join(appdata, "Propellerhead Software", "RackExtensions_Dev", rackExtensionName)
	if not os.path.isdir(installDir):
		os.makedirs(installDir)
	return installDir



#
# Build starter
#
def doBuild(arguments):
	usage = "build.py [Mac | Win] [[local45 [32 | 64 | i386 | x86_64 | Win32 | x64 | all] [Debugging | Testing | Deployment]] | [universal45]] "

	if len(arguments) < 2:
		print usage
		exit(1)

	osType = arguments[1]
	if osType != "Mac" and osType != "Win":
		print usage
		exit(1)

	target = arguments[2]

	try:
		infoLuaPath = "info.lua"
		if target == "optimized45":
			if len(arguments) != 8:
				print usage
				exit(1)
			inputDir = arguments[6]
			infoLuaPath = os.path.join(inputDir, "info.lua")


		luaFile = "lua"
		if osType == "Win":
			luaFile = luaFile + ".exe"
		luaFilePath = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "Tools", "Build", "Lua", osType, luaFile)
		luaScriptPath = os.path.join(buildconfig.JUKEBOX_SDK_DIR, "Tools", "Build", "Lua", "getproductid.lua")

		proc = subprocess.Popen([luaFilePath, luaScriptPath, infoLuaPath], stdout=subprocess.PIPE)
		productID = proc.communicate()[0]
		# There will be a trailing line break in the output from lua, remove it!
		productID = productID.rstrip()
		if proc.returncode != 0:
			print "Error in info.lua!"
			raise Exception()

		verbosePrint("product_id: " + productID)
		if not buildconfig.RACK_EXTENSION_NAME:
			# Normal(SDK) case, use name from last part of productID
			name = productID.split(".")[-1].strip()
			buildconfig.RACK_EXTENSION_NAME = name

		verbosePrint("RACK_EXTENSION_NAME: " + buildconfig.RACK_EXTENSION_NAME)

		if target == "local45":
			if len(arguments) != 5:
				print usage
				exit(1)
			platform = parsePlatformArgument(arguments[3])
			if platform == -1:
				print usage
				exit(1)
			if platform == 0 and osType == "Win":
				print r"Build 'all' architectures does not work on windows currently"
				exit(1)
			configuration = arguments[4]
			if not configuration in ["Deployment", "Debugging", "Testing"]:
				print usage
				exit(1)

			if platform == 0:
				local45All(buildconfig.RACK_EXTENSION_NAME, configuration, osType, buildconfig.OTHER_COMPILER_FLAGS, productID)
			else:
				local45(buildconfig.RACK_EXTENSION_NAME, platform, configuration, osType, buildconfig.OTHER_COMPILER_FLAGS, productID)
			print "Build finished"

		elif target == "universal45":
			if len(arguments) != 3:
				print usage
				exit(1)
			universal45(buildconfig.RACK_EXTENSION_NAME, osType, buildconfig.OTHER_COMPILER_FLAGS)
			print "Build finished"
		
		elif target == "optimized45":
			if len(arguments) != 8:
				print usage
				exit(1)
			platform = parsePlatformArgument(arguments[3])
			if platform == -1:
				print usage
				exit(1)
			configuration = arguments[4]
			if not configuration in ["Deployment", "Debugging", "Testing"]:
				print usage
				exit(1)
			rackExtensionName = arguments[5]
			inputDir = arguments[6]
			outputDir = arguments[7]
			if platform == 0:
				optimized45LibsAll(rackExtensionName, inputDir, outputDir, configuration, osType)
			else:
				optimized45Libs(rackExtensionName, inputDir, outputDir, platform, configuration, osType)

		elif target == "copyresources":
			if len(arguments) != 3:
				print usage
				exit(1)
			installDir = getInstallDir(buildconfig.RACK_EXTENSION_NAME)
			copyResources(osType, installDir)

		elif target == "copylibrary":
			if len(arguments) != 5:
				print usage
				exit(1)
			platform = parsePlatformArgument(arguments[3])
			if platform == -1:
				print usage
				exit(1)
			builtLibraryPath = arguments[4]
			installDir = getInstallDir(buildconfig.RACK_EXTENSION_NAME)
			copyLibrary(builtLibraryPath, buildconfig.RACK_EXTENSION_NAME, osType, platform, installDir, productID)

		else:
			print usage
	except SystemExit as e:
		if not e.code == 1:
			print "Build ERROR!"
			print traceback.format_exc()
			sys.exit(e.code)
		else:
			sys.exit(1)
	except:
		print "Build ERROR!"
		print traceback.format_exc()
		sys.exit(1)

