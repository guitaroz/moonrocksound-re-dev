--[[

An update to an already released Rack Extension must obey the following rules (applies
to hdgui.lua) to make sure it is song and patch compatible with content created using 
the released version.

--]]

jbox={}
global_all_trim_knobs={}
global_all_audio_inputs={}
global_all_audio_outputs={}
global_all_cv_inputs={}
global_all_cv_outputs={}
front={}
back={}
folded_front={}
folded_back={}

--
function table_contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

--
function jbox.panel(x) 
	return {graphics={},widgets={}}
end

function jbox.ui_text(x) return {}  end
function jbox.analog_knob(x) return {}  end
function jbox.zero_snap_knob(x) return {}  end
function jbox.pitch_wheel(x) return {}  end
function jbox.toggle_button(x) return {}  end
function jbox.momentary_button(x) return {}  end
function jbox.step_button(x) return {}  end
function jbox.radio_button(x) return {}  end
function jbox.up_down_button(x) return {}  end
function jbox.popup_button(x) return {}  end
function jbox.sequence_fader(x) return {} end
function jbox.sequence_meter(x) return {} end
function jbox.value_display(x) return {}  end
function jbox.device_name(x) return {}  end
function jbox.custom_display(x) return {}  end
function jbox.static_decoration(x) return {}  end

function jbox.audio_input_socket(x) 
	global_all_audio_inputs[x.socket] = x.socket
	return {}  
end

function jbox.audio_output_socket(x) 
	global_all_audio_outputs[x.socket] = x.socket
	return {}  
end

function jbox.cv_input_socket(x) 
	global_all_cv_inputs[x.socket] = x.socket
	return {}  
end

function jbox.cv_output_socket(x) 
	global_all_cv_outputs[x.socket] = x.socket
	return {}  
end

function jbox.patch_browse_group(x) return {}  end
function jbox.patch_name(x) return {}  end
function jbox.placeholder(x) return {}  end
function jbox.custom_display_backlit_lcd(x) return {}  end
function jbox.custom_display_frontlit_lcd(x) return {}  end

function jbox.cv_trim_knob(x) 
	global_all_trim_knobs[x.socket] = x.socket
	return {}
end

-- Reset all important global variables that are set during the loading of a hdgui.lua
function reset()
	front={}
	back={}
	folded_front={}
	folded_back={}
	global_all_trim_knobs={}
	global_all_audio_inputs={}
	global_all_audio_outputs={}
	global_all_cv_inputs={}
	global_all_cv_outputs={}
end


-- This is where this script starts to run..
if (arg[2] == nil) then
	print()
	print(" " .. arg[0] .. " is a utility to verify that a new Jukebox gui script")
	print(" is compatible with an old gui script.")
	print()
	print(" Usage: " .. arg[0] .. " <path_to_old_hdgui.lua> <path_to_new_hdgui.lua>")
	return
end

-- Load hdgui.lua numero 1
print("Loading file 1")
reset()
dofile(arg[1])
global_all_trim_knobs1 = global_all_trim_knobs
global_all_audio_inputs1 = global_all_audio_inputs
global_all_audio_outputs1 = global_all_audio_outputs
global_all_cv_inputs1 = global_all_cv_inputs
global_all_cv_outputs1 = global_all_cv_outputs

-- Load hdgui.lua numero 2
print("Loading file 2")
reset()
dofile(arg[2])
global_all_trim_knobs2 = global_all_trim_knobs
global_all_audio_inputs2 = global_all_audio_inputs
global_all_audio_outputs2 = global_all_audio_outputs
global_all_cv_inputs2 = global_all_cv_inputs
global_all_cv_outputs2 = global_all_cv_outputs

all_is_fine = true

-- Check that no trim knobs have been removed
for key, value in pairs(global_all_trim_knobs1) do
	if not table_contains(global_all_trim_knobs2, key) then
		print("\nThe trim knob associated with socket '" .. value .. "' is missing.")
		all_is_fine = false
	end
end

-- Check that no audio inputs have been removed
for key, value in pairs(global_all_audio_inputs1) do
	if not table_contains(global_all_audio_inputs2, key) then
		print("\nThe audio input socket for '" .. value .. "' is missing.")
		all_is_fine = false
	end
end

-- Check that no audio outputs have been removed
for key, value in pairs(global_all_audio_outputs1) do
	if not table_contains(global_all_audio_outputs2, key) then
		print("\nThe audio output socket for '" .. value .. "' is missing.")
		all_is_fine = false
	end
end

-- Check that no cv inputs have been removed
for key, value in pairs(global_all_cv_inputs1) do
	if not table_contains(global_all_cv_inputs2, key) then
		print("\nThe cv input socket for '" .. value .. "' is missing.")
		all_is_fine = false
	end
end

-- Check that no cv outputs have been removed
for key, value in pairs(global_all_cv_outputs1) do
	if not table_contains(global_all_cv_outputs2, key) then
		print("\nThe cv output socket for '" .. value .. "' is missing.")
		all_is_fine = false
	end
end

if (all_is_fine) then
	print("All is fine.")
else
	print("\nThere is a compatibility issue.")
	os.exit(1)
end

