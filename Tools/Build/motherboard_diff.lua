--[[

An update to an already released Rack Extension must obey the following rules to make
sure it is song and patch compatible with content created using the released version.

Rules:
1. A newer motherboard version must contain at least the same (stored in patch/song) 
   parameters as the old one.
2. These parameters must be of the same type.
3. If the type is a stepped number, the number of steps must be the same or higher.
4. Property persistence may never go backwards, e.g. from song to none. The following
   persistances exist and this is how they may be changed between versions (or stay 
   as they were):
      none -> song -> patch
5. CV and audio inputs/outputs may never be removed in the new version - only added.
6. CV and audio inputs/outputs must be declared in the same order as in the old version
7. MIDI CC must never change for a property if it has once been declared.
8. A property that has been declared in the remote implementation chart must stay there
   forever and its internal_name may never be changed.

--]]

jbox={}

-- Routing types
function jbox.add_stereo_audio_routing_pair(x) end
function jbox.add_stereo_audio_routing_target(x) end
function jbox.add_mono_audio_routing_target(x) end
function jbox.add_cv_routing_target(x) end
function jbox.add_stereo_effect_routing_hint(x) end
function jbox.add_stereo_instrument_routing_hint(x) end
function jbox.add_mono_instrument_routing_hint(x) end

-- Placeholder device bypass routing
function jbox.set_effect_auto_bypass_routing(x) end

-- Inputs/outputs
function jbox.audio_input(x) 
	global_jboxtype_index = global_jboxtype_index+1
	return {type="audio_input", index=global_jboxtype_index} 
end
function jbox.audio_output(x) 
	global_jboxtype_index = global_jboxtype_index+1
	return {type="audio_output", index=global_jboxtype_index} 
end
function jbox.cv_input(x) 
	global_jboxtype_index = global_jboxtype_index+1
	return {type="cv_input", index=global_jboxtype_index} 
end
function jbox.cv_output(x) 
	global_jboxtype_index = global_jboxtype_index+1
	return {type="cv_output", index=global_jboxtype_index} 
end

-- Functions needed when declaring properties
function jbox.property_set(x)
	-- This stores all document owner properties in the global props when running a motherboard_def.lua
	props = x.document_owner.properties
	-- This stores all gui owner properties in the global props when running a motherboard_def.lua
	if x.gui_owner ~= nil then
		props_gui = x.gui_owner.properties
	end
end

function jbox.ui_text(x) return {} end
function jbox.trace(x) end

-- Property formatting
function jbox.format_integer_as_string(x) end
function jbox.format_number_as_string(x) end
function jbox.expand_template_string(x) end
function jbox.format_note_length_value_as_string(x) end
function jbox.ui_percent(x) return {} end
function jbox.ui_linear(x) return {} end
function jbox.ui_nonlinear(x) return {} end
function jbox.ui_selector(x) return {} end

-- The following property types are not saved in patches/songs so no need to check
function jbox.sample(x) end
function jbox.blob(x) end
function jbox.native_object(x) end
function jbox.performance_modwheel(x) end
function jbox.performance_pitchbend(x) end
function jbox.performance_sustainpedal(x) end
function jbox.performance_expression(x) end
function jbox.performance_breathcontrol(x) end
function jbox.performance_aftertouch(x) end

-- Property types that can be saved in patches/songs
function jbox.number(x)
	local pers = x.persistence
	if (pers == nil) then
		pers = "none"
	end
	
	if (x.steps ~= nil) then
		return {type="stepped_number", steps=x.steps, persistence=pers}
	end
	
	return {type="number", persistence=pers}
end

function jbox.boolean(x) 
	local pers = x.persistence
	if (pers == nil) then
		pers = "none"
	end
	
	return {type="boolean", persistence=pers}
end

function jbox.string(x) 
	return {type="string", persistence="patch"}
end

-- Reset all important global variables that are set during the loading of a motherboard_def.lua
function reset()
	props = {}
	props_gui = {}
	audio_inputs = {}
	audio_outputs = {}
	cv_inputs = {}
	cv_outputs = {}
	midi_implementation_chart = {}
	remote_implementation_chart = {}
	
	global_jboxtype_index = 0
end

-- return the lowest number found for field "index" out of all entries in table
function lowest_index_in_connection_table(table)

	magic_number = 7777

	low_index = magic_number

	for connection_name,connection_info in pairs(table) do
		if (connection_info.index < low_index) then
			low_index = connection_info.index
		end
	end

	if (low_index == magic_number) then
		low_index = 0
	end
	
	return low_index
end

--[[
	Validate inputs/outputs here
	!!! JE: this function also checks for the correct socket type, which is not a diff-test towards
	the previous motherboard file. That socket type check is also done by Recon starting from Be-Bop
]]--
function are_connections_ok(connection1, connection2, socket_type)
	local is_ok = true

--[[
	JE: each table entry has a index (increasing downwards with LOC)
	when processing each table, subtract the lowest index in that table
	to allow diffs where the wrong socket type has been replaced with the correct one
	See FogBUGZ case 214002
]]--

	base_index1 = lowest_index_in_connection_table(connection1)
	base_index2 = lowest_index_in_connection_table(connection2)

	for connection_name,connection_info1 in pairs(connection1) do
		connection_info2 = connection2[connection_name]
		if (connection_info2 == nil) then
			print("ERROR: " .. socket_type .. " <" .. connection_name .. "> is missing in the latest version.")
			is_ok = false
		elseif (socket_type ~= connection_info2.type) then 
			print("ERROR: " .. socket_type .. " <" .. connection_name .. "> is of wrong socket type (is a " .. connection_info2.type .. ").")
			is_ok = false
		else

			index1 = connection_info1.index - base_index1
			index2 = connection_info2.index - base_index2

			if (index1 ~= index2) then
				print("ERROR: " .. socket_type .. "s must be declared in the same order as in the last version. <" .. connection_name .. "> has moved")
				is_ok = false
			end
		end
	end
	return is_ok
end

function property_pair_ok(property_name, p_info1, p_info2)
	if (p_info2 == nil) then
		print("ERROR: Property <" .. property_name .. "> has been removed from the latest version.")
		return false
	elseif (p_info2.type ~= p_info1.type) then
		print("ERROR: Property <" .. property_name .. "> has changed type in the latest version.")
		return false
	elseif (p_info1.type=="stepped_number" and p_info1.steps > p_info2.steps) then
		print("ERROR: Property <" .. property_name .. "> (stepped number) has fewer steps in the latest version.")		
		return false
	elseif ( (p_info2.persistence == "none" and p_info1.persistence ~= "none") or
			 (p_info2.persistence == "song" and p_info1.persistence == "patch") ) then
		print("ERROR: Property <" .. property_name .. "> has persistence \"" .. p_info2.persistence .. "\" in the newest version but \"" .. p_info1.persistence .. "\" in the old version")		
		return false
	end
	return true
end

-- This is where this script starts to run..
if (arg[2] == nil) then
	print()
	print(" " .. arg[0] .. " is a utility to verify that a new Jukebox motherboard definition")
	print(" is song and patch compatible with an old motherboard definition.")
	print()
	print(" Usage: " .. arg[0] .. " <path_to_old_motherboard_def.lua> <path_to_new_motherboard_def.lua>")
	return
end

-- Load motherboard_def numero 1
reset()
dofile(arg[1])
props1=props
props_gui1=props_gui
audio_inputs1=audio_inputs
audio_outputs1=audio_outputs
cv_inputs1=cv_inputs
cv_outputs1=cv_outputs
midi_implementation_chart1=midi_implementation_chart
remote_implementation_chart1=remote_implementation_chart

-- Load motherboard_def numero 2
reset()
dofile(arg[2])
props2=props
props_gui2=props_gui
audio_inputs2=audio_inputs
audio_outputs2=audio_outputs
cv_inputs2=cv_inputs
cv_outputs2=cv_outputs
midi_implementation_chart2=midi_implementation_chart
remote_implementation_chart2=remote_implementation_chart

all_is_fine = true
-- Validate property requirements
for property_name,p_info1 in pairs(props1) do
	local p_info2 = props2[property_name]
	if not property_pair_ok(property_name, p_info1, p_info2) then
		all_is_fine = false
	end
end
-- Validate property requirements for GUI properties
for property_name,p_info1 in pairs(props_gui1) do
	if props_gui2 == nil then
		print("ERROR: The gui_owner property set object is empty in the latest version.")
		all_is_fine = false
	else
		local p_info2 = props_gui2[property_name]
		if not property_pair_ok(property_name, p_info1, p_info2) then
			all_is_fine = false
		end
	end
end

-- Validate inputs/outputs 
if (are_connections_ok(audio_inputs1, audio_inputs2, "audio_input") == false) then
	all_is_fine = false
end

if (are_connections_ok(audio_outputs1, audio_outputs2, "audio_output") == false) then
	all_is_fine = false
end

if (are_connections_ok(cv_inputs1, cv_inputs2, "cv_input") == false) then
	all_is_fine = false
end

if (are_connections_ok(cv_outputs1, cv_outputs2, "cv_output") == false) then
	all_is_fine = false
end

-- Validate MIDI CC chart
--
-- File 1				File 2				Result
-----------------------------------------------------------
-- chart exists			chart don't exist	Error
-- chart exists			chart exists		Compare charts
-- chart don't exist	chart don't exist	OK, continue
-- chart don't exist	chart exists		OK, continue
--
if midi_implementation_chart1.midi_cc_chart ~= nil and midi_implementation_chart2.midi_cc_chart == nil then
	print("ERROR: MIDI CC chart is missing.")
	all_is_fine = false
else
	if midi_implementation_chart1.midi_cc_chart ~= nil and midi_implementation_chart2.midi_cc_chart ~= nil then
		for midi_cc,property_path1 in pairs(midi_implementation_chart1.midi_cc_chart) do
			local property_path2 = midi_implementation_chart2.midi_cc_chart[midi_cc]
			if (property_path2 == nil) then
				print("ERROR: MIDI CC " .. midi_cc .. " has lost its mapping (used to be " .. property_path1 .. ").")
				all_is_fine = false
			elseif (property_path2 ~= property_path1) then
				print("ERROR: MIDI CC " .. midi_cc .. " has changed mapping from " .. property_path1 .. " to " .. property_path2 .. ".")
				all_is_fine = false
			end
		end
	end
end

-- Validate remote implementation chart
old_remote_is_broken = false
all_was_fine = all_is_fine
warningsOrErrors = {}
for property_path,r_info1 in pairs(remote_implementation_chart1) do
	local r_info2 = remote_implementation_chart2[property_path]
	if (r_info2 == nil) then
		table.insert(warningsOrErrors, "Remote mapping for " .. property_path .. " has been removed.")
		all_is_fine = false
	elseif (r_info2.internal_name ~= r_info1.internal_name) then
		-- Allow change of internal_name if it ends with whitespace. If it does, remotemaps won't work anyway so breaking backwards compatibility is OK
		if (r_info1.internal_name:match("%s", -1) == nil) then
			table.insert(warningsOrErrors, "Remote mapping for " .. property_path .. " has had its internal name changed from \"" .. r_info1.internal_name .. "\" to \"" .. r_info2.internal_name .. "\".")
			all_is_fine = false
		else
			print("INFORMATION: Fixing broken remote mapping for " .. property_path .. " from \"" .. r_info1.internal_name .. "\" to \"" .. r_info2.internal_name .. "\".")
			old_remote_is_broken = true
		end
	end
end

for i, message in ipairs(warningsOrErrors) do
	if(old_remote_is_broken) then
		print("WARNING: " .. message)
	else
		print("ERROR: " .. message)
	end
end

if(old_remote_is_broken) then
	all_is_fine = all_was_fine
end

for property_path,r_info2 in pairs(remote_implementation_chart2) do
	if (r_info2.internal_name:match("%s", -1) ~= nil) then
		print("ERROR: Remote mapping for " .. property_path .. " has an illegal value \"" .. r_info2.internal_name .. "\".")
		all_is_fine = false
	end
end

if (all_is_fine) then
	print("All is fine.")
else
	print("\nThere is a compatibility issue.")
	os.exit(1)
end

