# Extract libc and libcpp memory functions into Intermediate-native and compile them
#
# This script is run by VisualStudio project

import os
import shutil
import array
import re
import string
import zipfile
import sys
import glob
import stat
import traceback
import subprocess
import pipes

JUKEBOX_SDK_DIR=os.path.abspath("..\\..")

def runCommand(commandLine):
	print commandLine
	returnValue = os.system(commandLine)
	if returnValue != 0:
		raise Exception()


def makeDirs():
    if not os.path.exists("Intermediate-native"):
        runCommand("md Intermediate-native\\Debugging\\32\\compiled_native")
        runCommand("md Intermediate-native\\Deployment\\32\\compiled_native")
        runCommand("md Intermediate-native\\Debugging\\64\\compiled_native")
        runCommand("md Intermediate-native\\Deployment\\64\\compiled_native")


def removeDirs():
    if os.path.exists("Intermediate-native"):
        runCommand("RD /S /Q Intermediate-native")

def compileNative(BITS, TARGET, LLCARGS):
    CURRENT_DIR=os.path.abspath(".")
    LIBOBJPATH="lib\\phdsp"+BITS+"\\"+TARGET
    LIBTARGETLETTER=""
    TARGETSTRING="Deployment"
    if TARGET == "debug":
        LIBTARGETLETTER="d"
        TARGETSTRING="Debugging"

    WORKDIR=os.path.join(os.path.join("Intermediate-native", TARGETSTRING), BITS)
    os.chdir(WORKDIR)
    runCommand("md "+LIBOBJPATH)
    runCommand("\""+ARPATH+"\" x \""+os.path.join(LLVM_DIR, "Jukebox\\libc\\lib\\phdsp"+BITS+"\\libc"+LIBTARGETLETTER+".a\""))
    runCommand("\""+ARPATH+"\" x \""+os.path.join(LLVM_DIR, "Jukebox\\libcxx\\lib\\phdsp"+BITS+"\\libcpp"+LIBTARGETLETTER+".a\""))

    for FILE in ["malloc", "free", "stdexcept", "throw", "exception", "newdelete", "new", "tinfo"]:
        runCommand("\""+LLCPATH+"\" "+LLCARGS+" "+os.path.join(LIBOBJPATH, FILE+".o")+" -o compiled_native\\"+FILE+".obj")

    os.chdir(CURRENT_DIR)

        

# Find LLVM distribution
LLVM_DIR = ""
try:
	LLVM_DIR = os.path.join(JUKEBOX_SDK_DIR, os.path.normpath("Tools/LLVM"))
	llvmFile = file(os.path.join(LLVM_DIR, os.path.normpath("Win32/Deployment/llc.exe")), 'r')
	llvmFile.close()
except:
	# This option is only intended for PH-internal purposes
	LLVM_DIR = os.path.join(JUKEBOX_SDK_DIR, os.path.normpath("../../LLVM"))
	llvmFile = file(os.path.join(LLVM_DIR, os.path.normpath("Win32/Deployment/llc.exe")), 'r')
	llvmFile.close()
LLVM_DIR=os.path.normpath(LLVM_DIR)
print "Using " + LLVM_DIR


LLCPATH = os.path.join(LLVM_DIR, os.path.normpath("Win32/Deployment/llc.exe"))
ARPATH = os.path.join(LLVM_DIR, os.path.normpath("Win32/Deployment/llvm-ar.exe"))


LLCARGS32="-O2 -mtriple=i686-pc-win32-jbcrt -march=x86 -mcpu=pentium-m -mattr=+sse2 -filetype=obj"
LLCARGS64="-O2 -mtriple=x86_64-pc-win32-jbcrt -march=x86-64 -mcpu=x86-64 -mattr=+sse3 -filetype=obj"

if not os.path.exists("Intermediate-native"):
    removeDirs()
    makeDirs()

    compileNative("32", "debug", LLCARGS32)
    compileNative("64", "debug", LLCARGS64)
    compileNative("32", "release", LLCARGS32)
    compileNative("64", "release", LLCARGS64)

