#!/bin/sh

# Extract libc and libcpp memory functions into Intermediate-native and compile and assemble libphmem_native.a
# which is the libraries in native format. Then create the universal lib libphmem_native_universal.a
#
# This script is run by XCode project

LLVMPATH="`pwd`/../../../../LLVM"
if [ ! -d "$LLVMPATH" ]; then
    LLVMPATH="`pwd`/../../Tools/LLVM"
fi
if [ ! -d "$LLVMPATH" ]; then
    LLVMPATH="`pwd`/../../../../Parts/LLVM"
fi
if [ ! -d "$LLVMPATH" ]; then
    LLVMPATH="`pwd`/../../../Parts/LLVM"
fi


LLCDIR="${LLVMPATH}/Mac/Deployment/bin"


function make_lib() {
TARGET=$1
BITS=$2

if [[ "$BITS" == 32 ]]; then
    LLCARGS="-O2 -mtriple=i686-apple-darwin9.0.0 -march=x86 -mcpu=yonah -mattr=+sse3 -relocation-model=pic -filetype=obj -static-init-in-data"
else
    LLCARGS="-O2 -mtriple=x86_64-apple-darwin9.0.0 -march=x86-64 -mcpu=x86-64 -mattr=+sse3 -relocation-model=pic -filetype=obj -static-init-in-data"
    BITS=64
fi

if [[ "$TARGET" == Debugging ]]; then
    LIBOBJPATH=lib/phdsp${BITS}/debug
    LIBTARGETLETTER=d
else
    LIBOBJPATH=lib/phdsp${BITS}/release
    TARGET=Deployment
    LIBTARGETLETTER=
fi

INTERMEDIATEPATH=Intermediate-native/${TARGET}/${BITS}

echo Making native lib for ${TARGET} ${BITS} bits.


(
    mkdir -p ${INTERMEDIATEPATH}

    cd ${INTERMEDIATEPATH}
    
    if [ ! -f libphmem_native.a ]; then
	    mkdir -p compiled_native
	    (
		cd compiled_native
		mkdir -p ${LIBOBJPATH}
		ar x "${LLVMPATH}/Jukebox/libc/lib/phdsp${BITS}/libc${LIBTARGETLETTER}.a"
		ar x "${LLVMPATH}/Jukebox/libcxx/lib/phdsp${BITS}/libcpp${LIBTARGETLETTER}.a"

		for i in ${LIBOBJPATH}/malloc.o ${LIBOBJPATH}/realloc.o ${LIBOBJPATH}/free.o ${LIBOBJPATH}/new.o ${LIBOBJPATH}/newdelete.o
		do 
		    "${LLCDIR}/llc" ${LLCARGS} $i -o $(basename "$i")
		done
	    )

	    libtool -s -c -static -o libphmem_native.a compiled_native/malloc.o compiled_native/realloc.o compiled_native/free.o compiled_native/new.o compiled_native/newdelete.o
	    rm -r compiled_native
    fi
)
}


make_lib Debugging 64
make_lib Debugging 32
make_lib Deployment 64
make_lib Deployment 32

lipo -create Intermediate-native/Debugging/32/libphmem_native.a Intermediate-native/Debugging/64/libphmem_native.a -output Intermediate-native/Debugging/libphmem_native_universal.a
lipo -create Intermediate-native/Deployment/32/libphmem_native.a Intermediate-native/Deployment/64/libphmem_native.a -output Intermediate-native/Deployment/libphmem_native_universal.a
