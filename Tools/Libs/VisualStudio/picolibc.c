// Pico-implementation of libc math functions needed to test LLVM intrinsics and wrap Microsoft libc.

#if defined(_INTRINSICSTEST)
	// Empty implementation to link with instead of platform libc to 
	// check that we don't depend on anything else.
	double pow(double x, double y) { return 0; }
	double sqrt(double x) { return 0; }
	double sin(double x) { return 0; }
	double cos(double x) { return 0; }
	double log(double x) { return 0; }
	double log10(double x) { return 0; }
	double exp(double x) { return 0; }
	float powf(float x, float y) { return 0; }
	float sqrtf(float x) { return 0; }
	float sinf(float x) { return 0; }
	float cosf(float x) { return 0; }
	float logf(float x) { return 0; }
	float log10f(float x) { return 0; }
	float expf(float x) { return 0; }
	
	// Make crtglue work.
	int _fltused = 1;
#elif defined(_WRAPPER) && defined(_32BIT)
	// Float math functions do not exist in 32 bit MS libc. 
	// Use double version instead.
	double pow(double x, double y);
	double sqrt(double x);
	double sin(double x);
	double cos(double x);
	double log(double x);
	double log10(double x);
	double exp(double x);
	float powf(float x, float y) { return (float)pow(x,y); }
	float sqrtf(float x) { return (float)sqrt(x); }
	float sinf(float x) { return (float)sin(x); }
	float cosf(float x) { return (float)cos(x); }
	float logf(float x) { return (float)log(x); }
	float log10f(float x) { return (float)log10(x); }
	float expf(float x) { return (float)exp(x); }
#elif defined(_WRAPPER) && defined(_64BIT)
	// No math wrapping needed in 64 bit

	// Make 64 bit MS libc work.
	int _fltused = 1;
#else
	#error "You need to define either _INTRINSICSTEST or _WRAPPER properly."
#endif
