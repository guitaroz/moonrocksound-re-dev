#pragma once

#ifndef PROPELLERHEAD_SOFTWARE_JUKEBOX_ABI_H
#define PROPELLERHEAD_SOFTWARE_JUKEBOX_ABI_H

#ifndef JUKEBOX_API
#if defined(__GNUC__) && !defined(__x86_64__)
#define JUKEBOX_API __attribute__((cdecl))	
#elif _MSC_VER
#define JUKEBOX_API __cdecl
#else
#define JUKEBOX_API
#endif // _MSC_VER
#endif // ndef JUKEBOX_API

/**
	@file
	@brief This is the interface for the implementation of the ABI as exposed by the DLL ABI
	@details
		Passing structs on the stack between pure native code and LLVM - compiled code is not
		safe. This layer, being built by the LLVM toolchain, makes the interface for 45 developer
		nicer by keeping safe by-value semantics, but converts to by-pointer output arguments to
		the native compiled host.
		
		For API documentation, see Jukebox.h
*/

#include "../../../../API/JukeboxTypes.h"

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////	45 developer export hooks

void JUKEBOX_API ABI_Export_RenderRealtime(void* iInstanceData, const TJBox_PropertyDiff iPropertyDiffs[], TJBox_UInt32 iDiffCount);
void* JUKEBOX_API ABI_Export_CreateNativeObject(const char iOperation[], const TJBox_Value iParams[], TJBox_UInt32 iCount);


//////////////////////////		Toolbox

void JUKEBOX_API ABI_Toolbox_Assert(const char iFile[], TJBox_Int32 iLine, const char iFailedExpression[], const char iMessage[]);
void JUKEBOX_API ABI_Toolbox_Trace(const char iFile[], TJBox_Int32 iLine, const char iMessage[]);
void JUKEBOX_API ABI_Toolbox_TraceValues(const char iFile[], TJBox_Int32 iLine, const char iTemplate[], const TJBox_Value iValues[], TJBox_Int32 iValueCount);


///////////////////	Accessing 45's object model.

TJBox_ObjectRef JUKEBOX_API ABI_Toolbox_GetMotherboardObjectRef(const TJBox_ObjectName iPath);

TJBox_Tag JUKEBOX_API ABI_Toolbox_GetPropertyTag(const TJBox_PropertyRef* iProperty);
void JUKEBOX_API ABI_Toolbox_FindPropertyByTag(TJBox_ObjectRef iObject, TJBox_Tag iTag, TJBox_PropertyRef* oPropertyRef);

void JUKEBOX_API ABI_Toolbox_MakePropertyRef(TJBox_ObjectRef iObject, const TJBox_PropertyKey iKey, TJBox_PropertyRef* oPropertyRef);


/////////////////////	Load / store a property in moterboard object model (MOM).

void JUKEBOX_API ABI_Toolbox_LoadMOMProperty(const TJBox_PropertyRef* iProperty, TJBox_Value* oValue);
void JUKEBOX_API ABI_Toolbox_StoreMOMProperty(const TJBox_PropertyRef* iProperty, const TJBox_Value* iValue);

void JUKEBOX_API ABI_Toolbox_LoadMOMPropertyByTag(TJBox_ObjectRef iObject, TJBox_Tag iTag, TJBox_Value* oValue);
void JUKEBOX_API ABI_Toolbox_StoreMOMPropertyByTag(TJBox_ObjectRef iObject, TJBox_Tag iTag, const TJBox_Value* iValue);


/////////////////////	TJBox_Value.

TJBox_ValueType JUKEBOX_API ABI_Toolbox_GetType(const TJBox_Value* iValue);

TJBox_Float64 JUKEBOX_API ABI_Toolbox_GetNumber(const TJBox_Value* iValue);

TJBox_UInt32 JUKEBOX_API ABI_Toolbox_GetStringLength(const TJBox_Value* iValue);
void JUKEBOX_API ABI_Toolbox_GetSubstring(
	const TJBox_Value* iValue,
	TJBox_SizeT iStart,
	TJBox_SizeT iEnd,
	char oString[]);

TJBox_Bool JUKEBOX_API ABI_Toolbox_GetBoolean(const TJBox_Value* iValue);

void JUKEBOX_API ABI_Toolbox_GetNativeObjectRO(const TJBox_Value* iValue, void const* * oObject);
void JUKEBOX_API ABI_Toolbox_GetNativeObjectRW(const TJBox_Value* iValue, void* * oObject);

void JUKEBOX_API ABI_Toolbox_GetSampleInfo(const TJBox_Value* iValue, TJBox_SampleInfo* oSampleInfo);
#if JUKEBOX_API_VERSION >= 200
void JUKEBOX_API ABI_Toolbox_GetSampleMetaData(const TJBox_Value* iValue, TJBox_SampleMetaData* oSampleMetaData);
#endif // JUKEBOX_API_VERSION >= 200
void JUKEBOX_API ABI_Toolbox_GetSampleData(
	const TJBox_Value* iValue,
	TJBox_AudioFramePos iStartFrame,
	TJBox_AudioFramePos iEndFrame,
	TJBox_AudioSample oAudio[]);

void JUKEBOX_API ABI_Toolbox_GetBLOBInfo(const TJBox_Value* iValue, TJBox_BLOBInfo* oBLOBInfo);
void JUKEBOX_API ABI_Toolbox_GetBLOBData(
	const TJBox_Value* iValue,
	TJBox_SizeT iStart,
	TJBox_SizeT iEnd,
	TJBox_UInt8 oData[]);

void JUKEBOX_API ABI_Toolbox_GetDSPBufferInfo(const TJBox_Value* iValue, TJBox_DSPBufferInfo* oDSPBufferInfo);
void JUKEBOX_API ABI_Toolbox_GetDSPBufferData(
	const TJBox_Value* iValue,
	TJBox_AudioFramePos iStartFrame,
	TJBox_AudioFramePos iEndFrame,
	TJBox_AudioSample oAudio[]);

void JUKEBOX_API ABI_Toolbox_SetDSPBufferData(
	const TJBox_Value* iValue,
	TJBox_AudioFramePos iStartFrame,
	TJBox_AudioFramePos iEndFrame,
	const TJBox_AudioSample iAudio[]);

void JUKEBOX_API ABI_Toolbox_MakeNil(TJBox_Value* oValue);
void JUKEBOX_API ABI_Toolbox_MakeNumber(TJBox_Float64 iNumber, TJBox_Value* oValue);
void JUKEBOX_API ABI_Toolbox_MakeBoolean(TJBox_Bool iBoolean, TJBox_Value* oValue);

/////////////////////	Shorthands for common tasks.

TJBox_Float64 JUKEBOX_API ABI_Toolbox_LoadMOMPropertyAsNumber(TJBox_ObjectRef iObject, TJBox_Tag iTag);
void JUKEBOX_API ABI_Toolbox_StoreMOMPropertyAsNumber(
	TJBox_ObjectRef iObject, 
	TJBox_Tag iTag, 
	TJBox_Float64 iNumber);


/////////////////////  Memory alignment functions.

TJBox_Int32 JUKEBOX_API ABI_Toolbox_GetMemoryAlignment();
TJBox_Bool JUKEBOX_API ABI_Toolbox_IsMemoryAligned(void* iMemory);


/////////////////////  Accelerated DSP functions.

void JUKEBOX_API ABI_Toolbox_FFTRealForward(TJBox_Int32 iFFTSize, TJBox_Float32 ioData[]);
void JUKEBOX_API ABI_Toolbox_FFTRealInverse(TJBox_Int32 iFFTSize, TJBox_Float32 ioData[]);

#ifdef __cplusplus
}
#endif

#endif	// PROPELLERHEAD_SOFTWARE_JUKEBOX_ABI_H
