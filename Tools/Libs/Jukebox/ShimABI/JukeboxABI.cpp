#include "Jukebox.h"
#include "JukeboxABI.h"

 #include <string.h>

void JBox_Export_RenderRealtime(void* iInstanceData, const TJBox_PropertyDiff iPropertyDiffs[], TJBox_UInt32 iDiffCount);
void* JBox_Export_CreateNativeObject(const char iOperation[], const TJBox_Value iParams[], TJBox_UInt32 iParamCount);

////////////////////////////////	45 developer must implement these.

void JUKEBOX_API ABI_Export_RenderRealtime(void* iInstanceData, const TJBox_PropertyDiff iPropertyDiffs[], TJBox_UInt32 iDiffCount) {
	JBox_Export_RenderRealtime(iInstanceData, iPropertyDiffs, iDiffCount);
}

void* JUKEBOX_API ABI_Export_CreateNativeObject(const char iOperation[], const TJBox_Value iParams[], TJBox_UInt32 iParamCount) {
	return JBox_Export_CreateNativeObject(iOperation, iParams, iParamCount);
}
	

//////////////////////////		Toolbox

void JBox_Assert(const char iFile[], TJBox_Int32 iLine, const char iFailedExpression[], const char iMessage[]) {
	ABI_Toolbox_Assert(iFile, iLine, iFailedExpression, iMessage);
}

void JBox_Trace(const char iFile[], TJBox_Int32 iLine, const char iMessage[]) {
	ABI_Toolbox_Trace(iFile, iLine, iMessage);
}

void JBox_TraceValues(const char iFile[], TJBox_Int32 iLine, const char iTemplate[], const TJBox_Value iValues[], TJBox_Int32 iValueCount) {
	ABI_Toolbox_TraceValues(iFile, iLine, iTemplate, iValues, iValueCount);
}



///////////////////	Accessing 45's object model.

TJBox_ObjectRef JBox_GetMotherboardObjectRef(const TJBox_ObjectName iPath) {
	return ABI_Toolbox_GetMotherboardObjectRef(iPath);
}

TJBox_Tag JBox_GetPropertyTag(TJBox_PropertyRef iProperty) {
	return ABI_Toolbox_GetPropertyTag(&iProperty);
}

TJBox_PropertyRef JBox_FindPropertyByTag(TJBox_ObjectRef iObject, TJBox_Tag iTag) {
	TJBox_PropertyRef propertyRef;
	ABI_Toolbox_FindPropertyByTag(iObject, iTag, &propertyRef);
	return propertyRef;
}

TJBox_PropertyRef JBox_MakePropertyRef(TJBox_ObjectRef iObject, const TJBox_PropertyKey iKey) {
	TJBox_PropertyRef propertyRef;
	ABI_Toolbox_MakePropertyRef(iObject, iKey, &propertyRef);
	return propertyRef;
}

TJBox_Bool JBox_IsReferencingSameProperty(TJBox_PropertyRef iProperty1, TJBox_PropertyRef iProperty2) {
	return iProperty1.fObject == iProperty2.fObject
		&& 0 == strncmp(iProperty1.fKey, iProperty2.fKey, kJBox_MaxPropertyNameLen);
}


/////////////////////	Load / store a property in moterboard object model (MOM).

TJBox_Value JBox_LoadMOMProperty(const TJBox_PropertyRef iProperty) {
	TJBox_Value value;
	ABI_Toolbox_LoadMOMProperty(&iProperty, &value);
	return value;
}

void JBox_StoreMOMProperty(TJBox_PropertyRef iProperty, TJBox_Value iValue) {
	ABI_Toolbox_StoreMOMProperty(&iProperty, &iValue);
}

TJBox_Value JBox_LoadMOMPropertyByTag(TJBox_ObjectRef iObject, TJBox_Tag iTag) {
	TJBox_Value value;
	ABI_Toolbox_LoadMOMPropertyByTag(iObject, iTag, &value);
	return value;
}

void JBox_StoreMOMPropertyByTag(TJBox_ObjectRef iObject, TJBox_Tag iTag, TJBox_Value iValue) {
	ABI_Toolbox_StoreMOMPropertyByTag(iObject, iTag, &iValue);
}


/////////////////////	TJBox_Value.

TJBox_ValueType JBox_GetType(TJBox_Value iValue) {
	return ABI_Toolbox_GetType(&iValue);
}

TJBox_Float64 JBox_GetNumber(TJBox_Value iValue) {
	return ABI_Toolbox_GetNumber(&iValue);
}

TJBox_UInt32 JBox_GetStringLength(TJBox_Value iValue) {
	return ABI_Toolbox_GetStringLength(&iValue);
}

void JBox_GetSubstring(TJBox_Value iValue, TJBox_SizeT iStart, TJBox_SizeT iEnd, char oString[]) {
	ABI_Toolbox_GetSubstring(&iValue, iStart, iEnd, oString);
}

TJBox_Bool JBox_GetBoolean(TJBox_Value iValue) {
	return ABI_Toolbox_GetBoolean(&iValue);
}

const void* JBox_GetNativeObjectRO(TJBox_Value iValue) {
	const void* nativeObject = 0;
	ABI_Toolbox_GetNativeObjectRO(&iValue, &nativeObject);
	return nativeObject;
}

void* JBox_GetNativeObjectRW(TJBox_Value iValue) {
	void* nativeObject = 0;
	ABI_Toolbox_GetNativeObjectRW(&iValue, &nativeObject);
	return nativeObject;
}

TJBox_SampleInfo JBox_GetSampleInfo(TJBox_Value iValue) {
	TJBox_SampleInfo sampleInfo;
	ABI_Toolbox_GetSampleInfo(&iValue, &sampleInfo);
	return sampleInfo;
}

void JBox_GetSampleData(
	TJBox_Value iValue,
	TJBox_AudioFramePos iStartFrame,
	TJBox_AudioFramePos iEndFrame,
	TJBox_AudioSample oAudio[])
{
	ABI_Toolbox_GetSampleData(&iValue, iStartFrame, iEndFrame, oAudio);
}

TJBox_BLOBInfo JBox_GetBLOBInfo(TJBox_Value iValue) {
	TJBox_BLOBInfo blobInfo;
	ABI_Toolbox_GetBLOBInfo(&iValue, &blobInfo);
	return blobInfo;
}

void JBox_GetBLOBData(
	TJBox_Value iValue,
	TJBox_SizeT iStart,
	TJBox_SizeT iEnd,
	TJBox_UInt8 oData[])
{
	ABI_Toolbox_GetBLOBData(&iValue, iStart, iEnd, oData);
}

TJBox_DSPBufferInfo JBox_GetDSPBufferInfo(TJBox_Value iValue) {
	TJBox_DSPBufferInfo dspBufferInfo;
	ABI_Toolbox_GetDSPBufferInfo(&iValue, &dspBufferInfo);
	return dspBufferInfo;
}

void JBox_GetDSPBufferData(
	TJBox_Value iValue,
	TJBox_AudioFramePos iStartFrame,
	TJBox_AudioFramePos iEndFrame,
	TJBox_AudioSample oAudio[])
{
	ABI_Toolbox_GetDSPBufferData(&iValue, iStartFrame, iEndFrame, oAudio);
}

void JBox_SetDSPBufferData(
	TJBox_Value iValue,
	TJBox_AudioFramePos iStartFrame,
	TJBox_AudioFramePos iEndFrame,
	const TJBox_AudioSample iAudio[])
{
	ABI_Toolbox_SetDSPBufferData(&iValue, iStartFrame, iEndFrame, iAudio);
}

TJBox_Value JBox_MakeNil() {
	TJBox_Value value;
	ABI_Toolbox_MakeNil(&value);
	return value;
}

TJBox_Value JBox_MakeNumber(TJBox_Float64 iNumber) {
	TJBox_Value value;
	ABI_Toolbox_MakeNumber(iNumber, &value);
	return value;
}

TJBox_Value JBox_MakeBoolean(TJBox_Bool iBoolean) {
	TJBox_Value value;
	ABI_Toolbox_MakeBoolean(iBoolean, &value);
	return value;
}



/////////////////////	Shorthands for common tasks.

TJBox_Float64 JBox_LoadMOMPropertyAsNumber(TJBox_ObjectRef iObject, TJBox_Tag iTag) {
	return ABI_Toolbox_LoadMOMPropertyAsNumber(iObject, iTag);
}

void JBox_StoreMOMPropertyAsNumber(TJBox_ObjectRef iObject, TJBox_Tag iTag, TJBox_Float64 iValue) {
	ABI_Toolbox_StoreMOMPropertyAsNumber(iObject, iTag, iValue);
}

/////////////////////  Memory alignment functions

TJBox_Int32 JBox_GetMemoryAlignment() {
	return ABI_Toolbox_GetMemoryAlignment();
}

TJBox_Bool JBox_IsMemoryAligned(void* iMemory) {
	return ABI_Toolbox_IsMemoryAligned(iMemory);
}


/////////////////////  Accelerated DSP functions.

void JBox_FFTRealForward(TJBox_Int32 iFFTSize, TJBox_Float32 ioData[]) {
	ABI_Toolbox_FFTRealForward(iFFTSize, ioData);
}

void JBox_FFTRealInverse(TJBox_Int32 iFFTSize, TJBox_Float32 ioData[]) {
	ABI_Toolbox_FFTRealInverse(iFFTSize, ioData);
}
