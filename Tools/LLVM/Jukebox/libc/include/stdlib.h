/*
 * stdlib.h
 *
 * Definitions for common types, variables, and functions.
 */

#ifndef _STDLIB_H_
#define _STDLIB_H_

#include "machine/ieeefp.h"
#include "_ansi.h"

#define __need_size_t
#define __need_wchar_t
#include <stddef.h>

#if 0
#include <sys/reent.h>
#include <machine/stdlib.h>
#ifndef __STRICT_ANSI__
#include <alloca.h>
#endif
#endif /* 0 */

#ifdef __CYGWIN__
#include <cygwin/stdlib.h>
#endif

_BEGIN_STD_C

typedef struct 
{
  int quot; /* quotient */
  int rem; /* remainder */
} div_t;

typedef struct 
{
  long quot; /* quotient */
  long rem; /* remainder */
} ldiv_t;

#ifndef __STRICT_ANSI__
typedef struct
{
  long long int quot; /* quotient */
  long long int rem; /* remainder */
} lldiv_t;
#endif

#ifndef NULL
#define NULL 0
#endif

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define RAND_MAX __RAND_MAX

_VOID	_EXFUN(abort,(_VOID) _NEWLIB_ATTRIBUTE ((noreturn)));
int	_EXFUN(abs,(int));

div_t	_EXFUN(div,(int __numer, int __denom));

_VOID	_EXFUN_NOTHROW(free,(_PTR));

long	_EXFUN(labs,(long));
ldiv_t	_EXFUN(ldiv,(long __numer, long __denom));

_PTR	_EXFUN_NOTHROW(malloc,(size_t __size));

int	_EXFUN(rand,(_VOID));

_PTR	_EXFUN_NOTHROW(realloc,(_PTR __r, size_t __size));

_VOID	_EXFUN(srand,(unsigned __seed));


_END_STD_C

#endif /* _STDLIB_H_ */
