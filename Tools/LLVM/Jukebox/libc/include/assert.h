/*
	assert.h
*/

#ifdef __cplusplus
extern "C" {
#endif /* def __cplusplus */

#undef assert

#ifdef NDEBUG           /* required by ANSI standard */
# define assert(__e) ((void)0)
#else /* !NDEBUG */
# define assert(__e) ((__e) ? (void)0 : __assert_func(__FILE__, __LINE__, #__e))
#endif /* !NDEBUG */

void __assert_func(const char*, int, const char *);

#ifdef __cplusplus
}
#endif /* def __cplusplus */
