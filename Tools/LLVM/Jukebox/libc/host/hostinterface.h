#ifndef __hostinterface_h__
#define __hostinterface_h__

#include "hosttypes.h"

#if __cplusplus
extern "C" {
#endif /* __cplusplus */

void * HostInterface_stdlib_malloc(dsp_size_t);
void HostInterface_stdlib_free(void *);
void * HostInterface_stdlib_realloc(void *, dsp_size_t);
void HostInterface_stdlib_exit(int);
void HostInterface_stdlib_error(const char *);

#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __hostinterface_h__ */
