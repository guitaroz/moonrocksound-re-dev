#ifndef __hosttypes_h__
#define __hosttypes_h__

#if __phdsp32__
typedef unsigned int dsp_size_t; // 32-bit unsigned 
#elif __phdsp64__
typedef unsigned int dsp_size_t; // 64-bit unsigned
#elif _MSC_VER

#if _M_IX86
typedef unsigned __int32 dsp_size_t;
#elif _M_X64
typedef unsigned __int64 dsp_size_t;
#endif // 

#elif __GNUC__
#include <stdint.h>
#if __LP64__
typedef uint64_t dsp_size_t;
#else 
typedef uint32_t dsp_size_t;
#endif

#else
#error "Unknown host"
#endif

#endif // __hosttypes_h__
