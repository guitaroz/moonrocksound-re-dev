#ifndef cxxhostinterface_h
#define cxxhostinterface_h

// RP: We use hosttypes from libc since we depend on libc anyway.
#include "hosttypes.h"

#if __cplusplus
extern "C" {
#endif /* __cplusplus */

  void * HostInterfaceCPP_allocate_exception(dsp_size_t size);
  void HostInterfaceCPP_throw_exception(void*,void*,void(*)(void*));
  void HostInterfaceCPP_rethrow_exception();

#if __cplusplus
}
#endif /* __cplusplus */


#endif /* cxxhostinterface_h */
