/*  Copyright (c) 2011 Propellerhead Software AB. All rights reserved. */

#pragma once

#ifndef PROPELLERHEAD_JUKEBOXTYPES_H
#define PROPELLERHEAD_JUKEBOXTYPES_H

/**
	@file
	@brief Jukebox API types and enums
*/

/*
	Platform abstractions for LLVM and native targets
*/

#if __phdsp__
	typedef unsigned char TJBox_Bool;	/*	TRUE or FALSE */

	typedef signed char TJBox_Int8;
	typedef unsigned char TJBox_UInt8;

	typedef signed short TJBox_Int16;
	typedef unsigned short TJBox_UInt16;

	typedef float TJBox_Float32;
	typedef double TJBox_Float64;

	typedef signed int TJBox_Int32;
	typedef unsigned int TJBox_UInt32;

	#if __phdsp32__
	typedef signed long long TJBox_Int64;
	typedef unsigned long long TJBox_UInt64;
	#elif __phdsp64__
	typedef signed long TJBox_Int64;
	typedef unsigned long TJBox_UInt64;
	#else
	#error "Undefined __phdsp__ sub-target!"
	#endif 

	typedef unsigned long TJBox_SizeT;
	typedef unsigned long TJBox_PtrSize;
	typedef signed long TJBox_PtrDiff;
#elif __GNUC__ /* __phdsp__ */
	typedef unsigned long TJBox_SizeT;
	typedef unsigned long TJBox_PtrSize;
	typedef long TJBox_PtrDiff;
	
	typedef unsigned char TJBox_Bool;	/*	TRUE or FALSE */
	
	typedef signed char TJBox_Int8;
	typedef unsigned char TJBox_UInt8;
	
	typedef signed short TJBox_Int16;
	typedef unsigned short TJBox_UInt16;
	
	typedef float TJBox_Float32;
	typedef double TJBox_Float64;
	
	/*
		RP: The below works for __LP64__ to, although long and long long are the same size
		they are not the same type and long long is compatible with std::int64_t
	*/
	
	typedef int TJBox_Int32;
	typedef unsigned int TJBox_UInt32;
	
	typedef long long TJBox_Int64;
	typedef unsigned long long TJBox_UInt64;	
#elif _MSC_VER
	typedef unsigned char TJBox_Bool;	/*	TRUE or FALSE */

	typedef signed char TJBox_Int8;
	typedef unsigned char TJBox_UInt8;

	typedef signed short TJBox_Int16;
	typedef unsigned short TJBox_UInt16;

	/* RP: To match the definition of std::int32_t and std::uint32_t */
	typedef long TJBox_Int32;
	typedef unsigned long TJBox_UInt32;

	typedef __int64 TJBox_Int64;
	typedef unsigned __int64 TJBox_UInt64;

	typedef float TJBox_Float32;
	typedef double TJBox_Float64;
#if _M_X64
	typedef TJBox_UInt64 TJBox_SizeT;
	typedef TJBox_UInt64 TJBox_PtrSize;
	typedef TJBox_Int64 TJBox_PtrDiff;
#else
	typedef TJBox_UInt32 TJBox_SizeT;
	typedef TJBox_UInt32 TJBox_PtrSize;
	typedef TJBox_Int64 TJBox_PtrDiff;
#endif /* _M_X64 */
#else
#error "Unknown compiler."
#endif /* !__phdsp__ */



/**
	@brief Type that specifies a position within an audio buffer.
	@details
		An audio frame consists of one sample for each channel in the
		buffer.
*/
typedef TJBox_Int32 TJBox_AudioFramePos;

/**
	@brief Type that represents a single-channel audio sample.
*/
typedef TJBox_Float32 TJBox_AudioSample;


/* //////////////////		Values
*/

/**
	@brief The maximum length of property names in Jukebox, not including
		the terminating zero.
*/
#define kJBox_MaxPropertyNameLen	35
/**
	@brief Type that represents a property key.
*/
typedef char TJBox_PropertyKey[kJBox_MaxPropertyNameLen + 1];

/**
	@brief The maximum length of Motherboard property set object names in Jukebox, 
		not including the terminating zero.
*/
#define kJBox_MaxObjectNameLen		63
/**
	@brief Type that represents a Motherboard property set object name.
*/
typedef char TJBox_ObjectName[kJBox_MaxObjectNameLen + 1];

/**
	@brief Dynamic value types.
*/
typedef enum {
	kJBox_Nil = 2,
	kJBox_Number,
	kJBox_String,
	kJBox_Boolean,
	kJBox_Sample,
	kJBox_BLOB,
	kJBox_DSPBuffer,
	kJBox_NativeObject,
	kJBox_Incompatible
} TJBox_ValueType;

/**
 	@brief States for the "custom_properties/builtin_onoffbypass" property.
 */
typedef enum {
	kJBox_EnabledOff = 0,
	kJBox_EnabledOn,
	kJBox_EnabledBypass
} TJBox_OnOffBypassStates;

/**
 	@brief Tag for accessing the "custom_properties/builtin_onoffbypass" 
 		property.
 */
typedef enum {
	kJBox_CustomPropertiesOnOffBypass = 0xFFFFFFFD
} TJBox_CustomPropertyTag;

/**
	@brief Tags for accessing properties in the "transport" 
		Motherboard object.
*/
typedef enum {
	kJBox_TransportPlaying = 23,
	kJBox_TransportPlayPos,
	kJBox_TransportTempo,
	kJBox_TransportFilteredTempo,
	kJBox_TransportTempoAutomation,
	kJBox_TransportTimeSignatureNumerator,
	kJBox_TransportTimeSignatureDenominator,
	kJBox_TransportRequestResetAudio
} TJBox_TransportTag;

/**
	@brief Tags for accessing properties in the "environment" 
		Motherboard object.
 */
typedef enum {
	kJBox_EnvironmentMasterTune = 43,
	kJBox_EnvironmentSystemSampleRate,
	kJBox_EnvironmentInstanceID,
	kJBox_EnvironmentDeviceVisible
} TJBox_EnvironmentTag;

/**
	@brief Tags for accessing properties in the "audio_input" 
		Motherboard object.
 */
typedef enum {
	kJBox_AudioInputBuffer = 73,
	kJBox_AudioInputConnected
} TJBox_AudioInputTag;

/**
	@brief Tags for accessing properties in the "audio_output" 
		Motherboard object.
 */
typedef enum {
	kJBox_AudioOutputBuffer = 83,
	kJBox_AudioOutputConnected,
	kJBox_AudioOutputDSPLatency
} TJBox_AudioOutputTag;

/**
	@brief Tags for accessing properties in the "cv_input" 
		Motherboard object.
 */
typedef enum {
	kJBox_CVInputValue = 93,
	kJBox_CVInputConnected = 95
} TJBox_CVInputTag;

/**
	@brief Tags for accessing properties in the "cv_output" 
		Motherboard object.
 */
typedef enum {
	kJBox_CVOutputValue = 103,
	kJBox_CVOutputConnected,
	kJBox_CVOutputDSPLatency
} TJBox_CVOutputTag;

/**
	@brief Information about BLOB dynamic values.
*/
typedef struct {
    /** @brief The total size of the BLOB in bytes. */
	TJBox_SizeT fSize;
    /** @brief The number of bytes of the BLOB that are currently loaded in memory. */
	TJBox_SizeT fResidentSize;
} TJBox_BLOBInfo;


/**
	@brief Information about sample dynamic values.
*/
typedef struct {
    /** @brief The total number of frames in the sample. */
	TJBox_Int64 fFrameCount;
    /** @brief The number of frames of the sample that are currently loaded in memory. */
	TJBox_Int64 fResidentFrameCount;
    /** @brief The number of channels in the sample, e.g., 2 for stereo. */
	TJBox_UInt32 fChannels;
    /** @brief The sample rate of the sample in Hz. */
	TJBox_UInt32 fSampleRate;
} TJBox_SampleInfo;

/**
	@brief Information about DSP buffer dynamic values.
*/
typedef struct {
    /** @brief The total number of samples in the DSP buffer. */
	TJBox_Int64 fSampleCount;
} TJBox_DSPBufferInfo;

/**
	@brief Motherboard property set object reference.
*/
typedef TJBox_Int32 TJBox_ObjectRef;

/**
	@brief Property reference.
*/
typedef struct {
	/** @brief Property set object reference to the owner of this property. */ 
	TJBox_ObjectRef fObject;
	/**
		@brief Name of the property in the object as a zero-terminated ASCII string.
		@details Max length of the property name is kJBox_MaxPropertyNameLen, 
			not including the terminating zero.
	*/
	TJBox_PropertyKey fKey;
} TJBox_PropertyRef;

/**
	@brief Property tag.
*/
typedef TJBox_UInt32 TJBox_Tag;
/**
	@brief Invalid property tag.
*/
#define kJBox_InvalidPropertyTag ((TJBox_Tag)(-1))

/**
	@brief Opaque data type that holds a dynamic value.
	@details
		Dynamic values cannot be stored in C++ objects.
*/
typedef struct {
	TJBox_UInt8 fSecret[16];
} TJBox_Value;

/**
	@brief Represents a property change.
	@details
		Contains information about when the property was changed
		and its previous and current value.
*/
typedef struct {
	/** @brief The value of the property before the change. */
	TJBox_Value fPreviousValue;
	/** @brief The current value of the property. */
	TJBox_Value fCurrentValue;
	/** @brief Property reference. */
	TJBox_PropertyRef fPropertyRef;
	/** @brief The property tag if one has been assigned, 
		kJBox_InvalidPropertyTag otherwise. */
	TJBox_Tag fPropertyTag;
	/**
		@brief The frame in the batch where the change took place.
	    	This value will normally be between 0 and 63 but can, 
	    	because of a bug in Reason 6.5, be higher,
			in which case it can be safely treated as 63.
	*/
	TJBox_UInt16 fAtFrameIndex;
} TJBox_PropertyDiff;

#endif /* PROPELLERHEAD_JUKEBOXTYPES_H */

