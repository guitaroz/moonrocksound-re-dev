#pragma once
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "JukeboxTypes.h"

const size_t kBatchSize = 64;


// Note that we've used the property tag system here with a number mapping to motherboard_def.lua. When constructing the
// rack extension, you can also fetch property references via JBox_MakePropertyRef and the name
const TJBox_Tag kVolumePropertyTag = 42;
const TJBox_Tag kInputMeterTag = 43;
const TJBox_Tag kFilterCutoffPropertyTag = 44;
const TJBox_Tag kFilterResonancePropertyTag = 45;
const TJBox_Tag kMonitorOnPropertyTag = 46;
const TJBox_Tag kFilterTypePropertyTag = 47;
const TJBox_Tag kMixKnobPropertyTag = 48;
const TJBox_Tag kIntensityPropertyTag = 49;
const TJBox_Tag kMaxModPropertyTag = 50;
const TJBox_Tag kSpreadPropertyTag = 51;
const TJBox_Tag kBitDepthPropertyTag = 52;
const TJBox_Tag kBitRatePropertyTag = 53;
const TJBox_Tag kBitCrunchPostPropertyTag = 54;
const TJBox_Tag kBitCrunchDrivePropertyTag = 55;
const TJBox_Tag kKeyInSwitchPropertyTag = 56;
const TJBox_Tag kFoldbackSwitchPropertyTag = 57;




const size_t kFFTPowSize = 10;
const size_t kCircularBufferSize = 10000 * kBatchSize;

#endif
