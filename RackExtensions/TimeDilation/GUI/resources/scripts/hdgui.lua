format_version="1.0"

							

							--FRONT PANEL--

front = jbox.panel{
	graphics={ main_node="/root/panel" },
	widgets = {


--Utilities Group
	
		jbox.sequence_meter{
			graphics={
				main_node="/root/utilities/inputMeter/inputMeter/inputMeter",
			 },
 			value="/custom_properties/inputVol",
		},
		jbox.sequence_fader{
			graphics={
				main_node="/root/utilities/OnOffBypass/bypassFader",
				boundaries={left=-4, right=4, bottom=-7.5, top=7.5}
			},
			value="/custom_properties/builtin_onoffbypass",
			inset1 = 1.8,
			inset2 = 1.8,
			handle_size_mm = 5.5,
		},
		jbox.patch_browse_group{
			graphics={
 				main_node="/root/utilities/PatchBrowser/patchButts",
  			},
		},
		jbox.patch_name{
  			graphics={
    			main_node="/root/utilities/PatchBrowser/PatchDisp/patchDisplay/patchDisplay",
  			},
  			text_style="Bold LCD font",
			fg_color = {60, 255, 2},
  			loader_alt_color={0, 0, 0},
  			center=true,
  		},
		jbox.device_name{
 			graphics={
    			main_node="/root/Lscrews/tapeDisplacement/nametape",
  			},
		},


--Pitch Group
		
		jbox.value_display{
  			graphics={
    			main_node="/root/pitch/pitchRange/rangeDisplay/rangeDisplay",
  			},
  			value="/custom_properties/pitchRange",
  			text_style="Bold LCD font",
  			text_color={60, 255, 2},
  			loader_alt_color={0, 0, 0},
  			center=true
		},
		jbox.up_down_button{
			graphics={
				main_node="/root/pitch/pitchRange/rangeChange/rangeChange",
				boundaries={left=-3, right=2.9, bottom=-6.25, top=5.75}
			},
			value="/custom_properties/pitchRange",
		},
		jbox.pitch_wheel{
			graphics={
				main_node="/root/pitch/pitchWheel/pitch_wheel",
				boundaries={left=-8, right=8, bottom=-23, top=23}
			},
			value="/custom_properties/wheel",
		},
		jbox.sequence_fader{
			graphics={
				main_node="/root/pitch/mon/monitor_switch",
				boundaries={left=-10, right=11, bottom=-9, top=6},
			},
			value="/custom_properties/monitor",
			orientation = "horizontal",
			inset1 = 1,
			inset2 = 2,
			handle_size_mm = 5.5,
		},
		jbox.sequence_fader{
			graphics={
				main_node="/root/pitch/key/key_switch",
				boundaries={left=-10, right=11, bottom=-9, top=6},
			},
			value="/custom_properties/key",
			orientation = "horizontal",
			inset1 = 1,
			inset2 = 2,
			handle_size_mm = 5.5,
		},


--Mod Group

		jbox.sequence_fader{
			graphics={
				main_node="/root/mod/intensity/intensity_fader",
				boundaries={left=-112.6, right=12.6, bottom=-12.6, top=12.6}
			},
			value="/custom_properties/intensity",
  			orientation = "horizontal",
			inset1 = 6.9,
  			inset2 = 6.9,
  			handle_size_mm = 6.5
		},
		jbox.sequence_fader{
			graphics={
				main_node="/root/mod/maxMod/maxMod_fader",
				boundaries={left=-112.6, right=12.6, bottom=-12.6, top=12.6}
			},
			value="/custom_properties/maxMod",
 			orientation = "horizontal",
			inset1 = 6.9,
  			inset2 = 6.9,
  			handle_size_mm = 6.5
		},
		jbox.analog_knob{
			graphics={
				main_node="/root/mod/latency/latency_knob",
				boundaries={left=-6, right=6, bottom=-8, top=6}
			},
			value="/custom_properties/latency",
		},

		jbox.toggle_button{
			graphics={
				main_node="/root/mod/foldback/foldback_button",
				boundaries={left=-6, right=6, bottom=-6, top=6},
			},
			value="/custom_properties/foldback",
		},


--Filter Group

		jbox.sequence_fader{
			graphics={
				main_node="/root/filter/type/type_select",
				boundaries={left=-10, right=10, bottom=-8, top=5},
				hit_boundaries={left=-8, right=8, bottom=-8, top=5},

			},
			value="/custom_properties/filterType",
			orientation = "horizontal",
		},
		jbox.analog_knob{
			graphics={
				main_node="/root/filter/cutoff/cutoff_knob",
				boundaries={left=-13, right=14, bottom=-14, top=12}
			},
			value="/custom_properties/cutoff",
		},
		jbox.analog_knob{
			graphics={
				main_node="/root/filter/reso/resonance_knob",
				boundaries={left=-13, right=14, bottom=-14, top=12}
			},
			value="/custom_properties/resonance",
		},


--Bitcrush Group

		jbox.analog_knob{
			graphics={
				main_node="/root/bitcrush/drive/scaling/drive_knob",
				boundaries={left=-12, right=12, bottom=-12.75, top=11},
				hit_boundaries={left=-12, right=12, bottom=-12, top=11}
			},
			value="/custom_properties/inCrush",
		},
		jbox.analog_knob{
			graphics={
				main_node="/root/bitcrush/samp/scaling/rate_knob",
				boundaries={left=-12, right=12, bottom=-12.75, top=11},
				hit_boundaries={left=-12, right=12, bottom=-12, top=11}
			},
			value="/custom_properties/rateCrush",
		},
		jbox.analog_knob{
			graphics={
				main_node="/root/bitcrush/bit/scaling/depth_select",
				boundaries={left=-12, right=12, bottom=-12.75, top=11},
				hit_boundaries={left=-12, right=12, bottom=-12, top=11}
			},
			value="/custom_properties/depthCrush",
		},
		jbox.toggle_button{
			graphics={
				main_node="/root/bitcrush/prepost/prepost_button",
				boundaries={left=-6, right=6, bottom=-6, top=6},
		},
			value="/custom_properties/crushPos",
	},


--Output Group

		jbox.analog_knob{
			graphics={
				main_node="/root/output/mixKnob/mix/mix_knob",
				boundaries={left=-15, right=15, bottom=-16, top=14.5},
			},
			value="/custom_properties/mix",
		},
		jbox.analog_knob{
			graphics={
				main_node="/root/output/outputKnob/vol/vol_knob",
				boundaries={left=-12, right=12, bottom=-12.75, top=12}
			},
			value="/custom_properties/outputVol",
		},
		jbox.toggle_button{
			graphics={
				main_node="/root/output/spread/spreadButt",
				boundaries={left=-6, right=6, bottom=-6, top=6},
			},
			value="/custom_properties/spread",
		},

	}
}



						--BACK PANEL--

back = jbox.panel{
	graphics={ main_node="/root/panel" },
	widgets = {

--Input Group

		jbox.audio_input_socket{
  			graphics={
    			main_node="/root/MainInput/lIn",
  			},
  			socket="/audio_inputs/L_in",
  		},
  		jbox.audio_input_socket{
  			graphics={
    			main_node="/root/MainInput/rIn",
  			},
  			socket="/audio_inputs/R_in",
  		},

--Sidechain Group

		jbox.audio_input_socket{
  			graphics={
    			main_node="/root/SidechainInput/SClIn",
  			},
  			socket="/audio_inputs/L_SC",
  		},
  		jbox.audio_input_socket{
  			graphics={
    			main_node="/root/SidechainInput/SCrIn/SCrIn",
  			},
  			socket="/audio_inputs/R_SC",
  		},
  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/SidechainInput/SCcvIn/SCcvIn",
  			},
  			socket="/cv_inputs/cv_SC",
	},

--CV Group

	--Pitch Group
  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/pitchGroup/range",
  			},
  			socket="/cv_inputs/range_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { main_node="/root/CVinputGroup/pitchGroup/rangeTrim/rangeTrim"
 			},
  			socket = "/cv_inputs/range_cv_in" ,
		},

  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/pitchGroup/secondPitch/pitch",
  			},
  			socket="/cv_inputs/pitch_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { 
 				main_node="/root/CVinputGroup/pitchGroup/secondPitch/pitchTrim"
 			},
  			socket = "/cv_inputs/pitch_cv_in" ,
		},

	--Filter Group
  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/filterGroup/cutoff",
  			},
  			socket="/cv_inputs/cutoff_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { main_node="/root/CVinputGroup/filterGroup/cutoffTrim/cutoffTrim"
 			},
  			socket = "/cv_inputs/cutoff_cv_in" ,
		},

  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/filterGroup/secondFilter/reso",
  			},
  			socket="/cv_inputs/resonance_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { 
 				main_node="/root/CVinputGroup/filterGroup/secondFilter/resoTrim/resoTrim"
 			},
  			socket = "/cv_inputs/resonance_cv_in" ,
		},

	--Mod Group
  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/modGroup/intensity",
  			},
  		socket="/cv_inputs/intensity_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { main_node="/root/CVinputGroup/modGroup/intensityTrim/intensityTrim"
 			},
  		socket = "/cv_inputs/intensity_cv_in" ,
		},

  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/modGroup/secondMod/maxMod",
  			},
  		socket="/cv_inputs/maxMod_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { 
 				main_node="/root/CVinputGroup/modGroup/secondMod/maxModTrim/maxModTrim"
 			},
  		socket = "/cv_inputs/maxMod_cv_in" ,
		},

	--Crush Group
  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/crushGroup/resolution",
  			},
  		socket="/cv_inputs/resolution_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { main_node="/root/CVinputGroup/crushGroup/resolutionTrim/resolutionTrim"
 			},
  		socket = "/cv_inputs/resolution_cv_in" ,
		},
  		jbox.cv_input_socket{
  			graphics={
    			main_node="/root/CVinputGroup/crushGroup/secondCrush/depth",
  			},
  		socket="/cv_inputs/depth_cv_in",
	},
		jbox.cv_trim_knob{
 			graphics = { main_node="/root/CVinputGroup/crushGroup/secondCrush/depthTrim/depthTrim"
 			},
  		socket = "/cv_inputs/depth_cv_in" ,
		},


--Output Group

		jbox.audio_output_socket{
  			graphics={
    			main_node="/root/Outputs/lOut",
  			},
  		socket="/audio_outputs/L_out",
  		},
  		jbox.audio_output_socket{
  			graphics={
    			main_node="/root/Outputs/rOut/rOut",
  			},
  		socket="/audio_outputs/R_out",
  		},
  		 jbox.cv_output_socket{
  			graphics={
    			main_node="/root/Outputs/cvOut/cvOut",
  			},
  		socket="/cv_outputs/cv_out",
		},


--Utilities

		jbox.device_name{
 			graphics={
    			main_node="/root/nametape",
  			},
		},
		jbox.placeholder{
			graphics = { main_node="/root/placeholder" },
		},
	}
}






						--FOLDED FRONT--


folded_front = jbox.panel{
	graphics={ main_node="/root/panel" },
	widgets = {

		jbox.sequence_meter{
			graphics={
				main_node="/root/utilities/inputMeter/inputMeter",
			 },
 			value="/custom_properties/inputVol",
		},
		jbox.sequence_fader{
			graphics={
				main_node="/root/utilities/OnOffBypass/OnOffBypass",
				boundaries={left=-4, right=20, bottom=-7.5, top=7.5}
			},
			value="/custom_properties/builtin_onoffbypass",
			inset1 = 1.8,
			inset2 = 1.8,
			handle_size_mm = 5.5,
		},
		jbox.patch_browse_group{
			graphics={
 				main_node="/root/utilities/PatchBrowser/patchButts",
  			},
		},
		jbox.patch_name{
  			graphics={
    			main_node="/root/utilities/PatchBrowser/PatchDisp/patchDisplay/patchDisplay",
  			},
  			text_style="Bold LCD font",
			fg_color = {60, 255, 2},
  			loader_alt_color={0, 0, 0},
  			center=true,
  		},
		jbox.device_name{
 			graphics={
    			main_node="/root/utilities/nametape/nametape",
  			},
		},


	}
}




						--FOLDED BACK--

					
folded_back = jbox.panel{
	graphics={ main_node="/root/panel" },
	cable_origin = { main_node="/root/scaling/cable_origin" },
	widgets = {

		jbox.device_name{
 			graphics={
    			main_node="/root/Nametape/nametape/nametape",
  			},
		},

	}
}

