format_version = "1.0"
front = jbox.panel{
    backdrop = jbox.image{path = "Reason_GUI_front_root_panel"},
    widgets = {
        jbox.sequence_meter{
            transform = {41,36},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_utilities_inputMeter_inputMeter_inputMeter", frames = 11},
            value = "/custom_properties/inputVol",
        },
        jbox.sequence_fader{
            transform = {46,8},
            margins = {left = 1, top = 1, right = -24, bottom = 1},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_utilities_OnOffBypass_bypassFader", frames = 3},
            handle_size = 9.000000,
            inset1 = 3.000000,
            inset2 = 3.000000,
            value = "/custom_properties/builtin_onoffbypass",
        },
        jbox.patch_browse_group{
            transform = {88,10},
        },
        jbox.patch_name{
            transform = {145,9},
            center = true,
            fg_color = { 60, 255, 2 },
            height = 23.000000,
            loader_alt_color = { 0, 0, 0 },
            text_style = "Bold LCD font",
            width = 109.000000,
        },
        jbox.device_name{
            transform = {22,32},
            orientation = "vertical",
        },
        jbox.value_display{
            transform = {70,36},
            height = 22.000000,
            text_color = { 60, 255, 2 },
            text_style = "Bold LCD font",
            value = "/custom_properties/pitchRange",
            width = 19.000000,
        },
        jbox.up_down_button{
            transform = {93,38},
            margins = {left = 0, top = 0, right = 0, bottom = 1},
            background = jbox.image_sequence{path = "Reason_GUI_front_root_pitch_pitchRange_rangeChange_rangeChange", frames = 3},
            value = "/custom_properties/pitchRange",
        },
        jbox.pitch_wheel{
            transform = {67,59},
            margins = {left = 2, top = 3, right = 2, bottom = 3},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_pitch_pitchWheel_pitch_wheel", frames = 64},
            value = "/custom_properties/wheel",
        },
        jbox.sequence_fader{
            transform = {101,67},
            margins = {left = 6, top = 1, right = 8, bottom = 6},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_pitch_mon_monitor_switch", frames = 2},
            handle_size = 9.000000,
            inset1 = 2.000000,
            inset2 = 4.000000,
            orientation = "horizontal",
            value = "/custom_properties/monitor",
        },
        jbox.sequence_fader{
            transform = {101,104},
            margins = {left = 6, top = 2, right = 8, bottom = 6},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_pitch_key_key_switch", frames = 2},
            handle_size = 9.000000,
            inset1 = 2.000000,
            inset2 = 4.000000,
            orientation = "horizontal",
            value = "/custom_properties/key",
        },
        jbox.sequence_fader{
            transform = {143,47},
            margins = {left = 171, top = 11, right = 14, bottom = 12},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_mod_intensity_intensity_fader", frames = 64},
            handle_size = 11.000000,
            inset1 = 11.000000,
            inset2 = 11.000000,
            orientation = "horizontal",
            value = "/custom_properties/intensity",
        },
        jbox.sequence_fader{
            transform = {143,90},
            margins = {left = 171, top = 11, right = 14, bottom = 11},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_mod_maxMod_maxMod_fader", frames = 64},
            handle_size = 11.000000,
            inset1 = 11.000000,
            inset2 = 11.000000,
            orientation = "horizontal",
            value = "/custom_properties/maxMod",
        },
        jbox.analog_knob{
            transform = {341,57},
            margins = {left = 0, top = 0, right = 0, bottom = 4},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_mod_latency_latency_knob", frames = 63},
            value = "/custom_properties/latency",
        },
        jbox.toggle_button{
            transform = {341,100},
            margins = {left = 3, top = 3, right = 3, bottom = 3},
            background = jbox.image_sequence{path = "Reason_GUI_front_root_mod_foldback_foldback_button", frames = 4},
            value = "/custom_properties/foldback",
        },
        jbox.sequence_fader{
            transform = {431,14},
            margins = {left = 3, top = 0, right = 3, bottom = 0},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_filter_type_type_select", frames = 3},
            handle_size = 0.000000,
            orientation = "horizontal",
            value = "/custom_properties/filterType",
        },
        jbox.analog_knob{
            transform = {426,42},
            margins = {left = 3, top = 1, right = 5, bottom = 4},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_filter_cutoff_cutoff_knob", frames = 63},
            value = "/custom_properties/cutoff",
        },
        jbox.analog_knob{
            transform = {426,84},
            margins = {left = 3, top = 1, right = 5, bottom = 4},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_filter_reso_resonance_knob", frames = 63},
            value = "/custom_properties/resonance",
        },
        jbox.analog_knob{
            transform = {516,13},
            margins = {left = 0, top = 0, right = 0, bottom = 1},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_bitcrush_drive_scaling_drive_knob", frames = 63},
            value = "/custom_properties/inCrush",
        },
        jbox.analog_knob{
            transform = {516,53},
            margins = {left = 0, top = 0, right = 0, bottom = 1},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_bitcrush_samp_scaling_rate_knob", frames = 63},
            value = "/custom_properties/rateCrush",
        },
        jbox.analog_knob{
            transform = {516,93},
            margins = {left = 0, top = 0, right = 0, bottom = 1},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_bitcrush_bit_scaling_depth_select", frames = 63},
            value = "/custom_properties/depthCrush",
        },
        jbox.toggle_button{
            transform = {481,59},
            margins = {left = 3, top = 3, right = 4, bottom = 3},
            background = jbox.image_sequence{path = "Reason_GUI_front_root_bitcrush_prepost_prepost_button", frames = 4},
            value = "/custom_properties/crushPos",
        },
        jbox.analog_knob{
            transform = {587,23},
            margins = {left = -22, top = -23, right = -23, bottom = -19},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_output_mixKnob_mix_mix_knob", frames = 64},
            value = "/custom_properties/mix",
        },
        jbox.analog_knob{
            transform = {685,63},
            margins = {left = 1, top = 2, right = 2, bottom = 2},
            animation = jbox.image_sequence{path = "Reason_GUI_front_root_output_outputKnob_vol_vol_knob", frames = 63},
            value = "/custom_properties/outputVol",
        },
        jbox.toggle_button{
            transform = {689,44},
            margins = {left = 3, top = 3, right = 3, bottom = 3},
            background = jbox.image_sequence{path = "Reason_GUI_front_root_output_spread_spreadButt", frames = 4},
            value = "/custom_properties/spread",
        },
    }
}

folded_front = jbox.panel{
    backdrop = jbox.image{path = "Reason_GUI_front_folded_root_panel"},
    widgets = {
        jbox.sequence_meter{
            transform = {42,8},
            animation = jbox.image_sequence{path = "Reason_GUI_front_folded_root_utilities_inputMeter_inputMeter", frames = 4},
            value = "/custom_properties/inputVol",
        },
        jbox.sequence_fader{
            transform = {64,3},
            margins = {left = 1, top = 0, right = 1, bottom = 0},
            animation = jbox.image_sequence{path = "Reason_GUI_front_folded_root_utilities_OnOffBypass_OnOffBypass", frames = 3},
            handle_size = 9.000000,
            inset1 = 3.000000,
            inset2 = 3.000000,
            value = "/custom_properties/builtin_onoffbypass",
        },
        jbox.patch_browse_group{
            transform = {106,4},
        },
        jbox.patch_name{
            transform = {163,4},
            center = true,
            fg_color = { 60, 255, 2 },
            height = 22.000000,
            loader_alt_color = { 0, 0, 0 },
            text_style = "Bold LCD font",
            width = 109.000000,
        },
        jbox.device_name{
            transform = {555,8},
        },
    }
}

back = jbox.panel{
    backdrop = jbox.image{path = "Reason_GUI_back_root_panel"},
    widgets = {
        jbox.audio_input_socket{
            transform = {103,36},
            socket = "/audio_inputs/L_in",
        },
        jbox.audio_input_socket{
            transform = {129,36},
            socket = "/audio_inputs/R_in",
        },
        jbox.audio_input_socket{
            transform = {103,83},
            socket = "/audio_inputs/L_SC",
        },
        jbox.audio_input_socket{
            transform = {129,83},
            socket = "/audio_inputs/R_SC",
        },
        jbox.cv_input_socket{
            transform = {118,106},
            socket = "/cv_inputs/cv_SC",
        },
        jbox.cv_input_socket{
            transform = {198,23},
            socket = "/cv_inputs/range_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {218,21},
            socket = "/cv_inputs/range_cv_in",
        },
        jbox.cv_input_socket{
            transform = {198,47},
            socket = "/cv_inputs/pitch_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {218,44},
            socket = "/cv_inputs/pitch_cv_in",
        },
        jbox.cv_input_socket{
            transform = {198,75},
            socket = "/cv_inputs/cutoff_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {218,73},
            socket = "/cv_inputs/cutoff_cv_in",
        },
        jbox.cv_input_socket{
            transform = {198,98},
            socket = "/cv_inputs/resonance_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {218,96},
            socket = "/cv_inputs/resonance_cv_in",
        },
        jbox.cv_input_socket{
            transform = {284,23},
            socket = "/cv_inputs/intensity_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {304,21},
            socket = "/cv_inputs/intensity_cv_in",
        },
        jbox.cv_input_socket{
            transform = {284,47},
            socket = "/cv_inputs/maxMod_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {304,44},
            socket = "/cv_inputs/maxMod_cv_in",
        },
        jbox.cv_input_socket{
            transform = {284,75},
            socket = "/cv_inputs/resolution_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {304,73},
            socket = "/cv_inputs/resolution_cv_in",
        },
        jbox.cv_input_socket{
            transform = {284,98},
            socket = "/cv_inputs/depth_cv_in",
        },
        jbox.cv_trim_knob{
            transform = {304,96},
            socket = "/cv_inputs/depth_cv_in",
        },
        jbox.audio_output_socket{
            transform = {647,83},
            socket = "/audio_outputs/L_out",
        },
        jbox.audio_output_socket{
            transform = {674,83},
            socket = "/audio_outputs/R_out",
        },
        jbox.cv_output_socket{
            transform = {663,106},
            socket = "/cv_outputs/cv_out",
        },
        jbox.device_name{
            transform = {47,29},
            orientation = "vertical",
        },
        jbox.placeholder{
            transform = {595,12},
        },
    }
}

folded_back = jbox.panel{
    backdrop = jbox.image{path = "Reason_GUI_back_folded_root_panel"},
    cable_origin = {377,15},
    widgets = {
        jbox.device_name{
            transform = {555,8},
        },
    }
}

