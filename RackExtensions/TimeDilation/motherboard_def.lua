format_version = "1.0"

--CUSTOM PROPERTIES--
custom_properties = jbox.property_set{
    
    gui_owner = {
        properties = {
            
        }
    },
    
    document_owner = {
        properties = {
            
            --PITCH GROUP--
            
            pitchRange = jbox.number {
                steps = 13,
                default = 7,
                ui_name = jbox.ui_text("pitchRange"),
                ui_type = jbox.ui_selector{jbox.ui_text("0"), jbox.ui_text("1"), jbox.ui_text("2"), jbox.ui_text("3"), jbox.ui_text("4"), jbox.ui_text("5"), jbox.ui_text("6"), jbox.ui_text("7"), jbox.ui_text("8"), jbox.ui_text("9"), jbox.ui_text("10"), jbox.ui_text("11"), jbox.ui_text("12")},
            },
            monitor = jbox.boolean {
                property_tag = 46,
                default = false,
                ui_name = jbox.ui_text("monitor"),
                ui_type = jbox.ui_selector{jbox.ui_text("mod off"), jbox.ui_text("mod on")},
            },
            
            key = jbox.boolean {
                steps = 2,
                property_tag = 56,
                default = false,
                ui_name = jbox.ui_text("key"),
                ui_type = jbox.ui_selector{jbox.ui_text("main"), jbox.ui_text("sidechain")},
            },
            
            -- Pitch Wheel, will need to be reconfigured to not use the standard MIDI pitch bend function
            wheel = jbox.performance_pitchbend{},
            
            
            
            --MOD GROUP--
            -- Mod Intensity, needs to be reconfigured
            intensity = jbox.number{
                default = 0,
                property_tag = 49,
                ui_name = jbox.ui_text("intensity"),
                ui_type = jbox.ui_percent{
                    min = 0,
                    max = 100,
                    decimals = 0,
                },
            },
            
            -- MaxMod, needs to be reconfigured
            maxMod = jbox.number{
                property_tag = 50,
                default = 1,
                ui_name = jbox.ui_text("maxMod"),
                ui_type = jbox.ui_percent{
                    min = 0,
                    max = 100,
                    decimals = 0,
                },
            },
            -- Latency, needs to be reconfigured
            latency = jbox.number{
                default = 0.9,
                ui_name = jbox.ui_text("latency"),
                ui_type = jbox.ui_nonlinear{
                    -- convert data range 0-1 to dB value using an x^3 curve. Data value 0.7 is 0 dB.
                    data_to_gui = function(data_value)
                    local gain = math.pow(data_value / 0.7, 3)
                    local ui_value =  20 * math.log10(gain)
                    return ui_value
                    end,
                    -- convert UI dB value to data range 0-1
                    gui_to_data = function(gui_value)
                    local data_value = math.pow(math.pow(10, gui_value / 20), 1/3) * 0.7
                    return data_value
                    end,
                    units = {
                        {min_value=0, unit = {template=jbox.ui_text("ui_type template latency"), base=1}, decimals=2},
                    },
                },
            },
            foldback = jbox.boolean {
                property_tag = 57,
                default = false,
                ui_name = jbox.ui_text("foldback"),
                ui_type = jbox.ui_selector{jbox.ui_text("foldback off"), jbox.ui_text("foldback on")},
            },
            
            
            
            
            --FILTER GROUP--
            
            filterType = jbox.number {
                property_tag = 47,
                steps = 3,
                default = 2,
                ui_name = jbox.ui_text("filterType"),
                ui_type = jbox.ui_selector{jbox.ui_text("hpf"),jbox.ui_text("bpf"), jbox.ui_text("lpf")},
            },
            
            
            -- Filter Cutoff, needs to be reconfigured
            cutoff = jbox.number{
                property_tag = 44,
                default = .7,
                ui_name = jbox.ui_text("cutoff"),
                ui_type = jbox.ui_nonlinear{
                    -- convert data range 0-1 to Hz value using an x^3 curve.
                    data_to_gui = function(data_value)
                    local adapted_value = math.pow(data_value,3)
                    local ui_value = (adapted_value) * (16000 - 32) + 32
                    return ui_value
                    end,
                    -- For some reason this doesnt work. Doing conversion in code.
                    gui_to_data = function(gui_value)
                    return gui_value
                    end,
                    units = {
                        {min_value=0, unit = {template=jbox.ui_text("ui_type template cutoff"), base=1}, decimals=0},
                    },
                },		
                
            },
            
            
            -- Filter Resonance, needs to be reconfigured
            resonance = jbox.number{
                property_tag = 45,
                default = .3,
                ui_name = jbox.ui_text("resonance"),
                ui_type = jbox.ui_percent{
                    min = 0,
                    max = 100,
                    decimals = 0,
                },
            },
            
            
            
            
            
            
            
            --BITCRUSH GROUP--
            
            inCrush = jbox.number{
                default = 0.5,
                property_tag = 55,
                ui_name = jbox.ui_text("inCrush"),
                ui_type = jbox.ui_nonlinear{
                    -- convert data range 0-1 to dB value using an x^3 curve. Data value 0.7 is 0 dB.
                    data_to_gui = function(data_value)
                    --local gain = math.pow(data_value / 0.7, 3)
                    local ui_value =  data_value * 60 - 30    -- 20 * math.log10(gain)
                    return ui_value
                    end,
                    -- convert UI dB value to data range 0-1
                    gui_to_data = function(gui_value)
                    local data_value = gui_value * 60 - 30 -- math.pow(math.pow(10, gui_value / 20), 1/3) * 0.7
                    return data_value
                    end,
                    units = {
                        {min_value=0, unit = {template=jbox.ui_text("ui_type template inCrush"), base=1}, decimals=2},
                    },
                },
            },
            
            -- Bit Depth
            depthCrush = jbox.number{
                default = 20,
                property_tag = 52,
                steps =  21,
                ui_name = jbox.ui_text("depthCrush"),
                ui_type = jbox.ui_linear{
                    
                    min = 3,
                    max = 24,
                    units = {
                        {min_value=0, unit = {template=jbox.ui_text("ui_type template depthCrush"), base=1}, decimals=0},
                    },
                },
            },
            
            -- Sample Rate
            rateCrush = jbox.number{
                default = 13951,
                property_tag = 53,
                steps = 13952,
                ui_name = jbox.ui_text("rateCrush"),
                ui_type = jbox.ui_linear{
                    
                    min = 2048,
                    max = 16000,
                    units = {
                        {min_value=0, unit = {template=jbox.ui_text("ui_type template rateCrush"), base=1}, decimals=0},
                    },
                },
            },
            
            -- Bit Crush pre/pst filter, needs to be reconfigured
            crushPos = jbox.number {
                property_tag = 54,
                steps = 2,
                default = 0,
                ui_name = jbox.ui_text("pre/post"),
                ui_type = jbox.ui_selector{jbox.ui_text("post"), jbox.ui_text("pre")},
            },
            
            
            --OUTPUT GROUP--
            
            -- Mix Knob, needs to be reconfigured
            mix = jbox.number{
                default = 1,
                property_tag = 48,
                ui_name = jbox.ui_text("mix"),
                ui_type = jbox.ui_percent{
                    min = 0,
                    max = 100,
                    decimals = 0,
                },
            },
            
            outputVol = jbox.number{
                default = 0.7,
                property_tag = 42,
                ui_name = jbox.ui_text("outputVol"),
                ui_type = jbox.ui_nonlinear{
                    -- convert data range 0-1 to dB value using an x^3 curve. Data value 0.7 is 0 dB.
                    data_to_gui = function(data_value)
                    local gain = math.pow(data_value / 0.7, 3)
                    local ui_value =  20 * math.log10(gain)
                    return ui_value
                    end,
                    -- convert UI dB value to data range 0-1
                    gui_to_data = function(gui_value)
                    local data_value = math.pow(math.pow(10, gui_value / 20), 1/3) * 0.7
                    return data_value
                    end,
                    units = {
                        {min_value=0, unit = {template=jbox.ui_text("ui_type template outputVol"), base=1}, decimals=2},
                    },
                },
                persistence = "song"
            },
            
            spread = jbox.boolean {
                property_tag = 51,
                default = false,
                ui_name = jbox.ui_text("spread"),
                ui_type = jbox.ui_selector{jbox.ui_text("spread off"), jbox.ui_text("spread on")},
            },
        }
    },
    
    rtc_owner = {
        properties = {
            instance = jbox.native_object{}
        }
    },
    
    rt_owner = {
        properties = {
            -- Input Meter, needs to be reconfigured
            inputVol = jbox.number{
                default = 0.0,
                property_tag = 43,
                ui_name = jbox.ui_text("inputVol"),
                ui_type = jbox.ui_percent{
                    min = 0,
                    max = 1,
                    decimals = 5,
                },
            },
        }
    }
}


midi_implementation_chart = {
    
}


remote_implementation_chart = {
    ["/custom_properties/pitchRange"] = {
        internal_name = "Pitch Bend Range",   -- Max 64 chars
        short_ui_name = jbox.ui_text("short pitchRange"),          -- Max 8 chars
        shortest_ui_name = jbox.ui_text("shortest pitchRange")     -- Max 4 chars
    },
    
    
}


ui_groups = {
    {
        -- We have so few properties we don't need grouping. Consider for your project though
        ui_name = jbox.ui_text("group name not used"),
        properties = {
            "/custom_properties/builtin_onoffbypass",
        }
    },
}

--CV INPUTS
cv_inputs = {
    
    --Sidechain
    cv_SC = jbox.cv_input{
        ui_name = jbox.ui_text("sidechain cv input")
    },
    
    --Pitch Group
    range_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("pitch bend range cv in")
    },
    pitch_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("pitch bend amt cv in")
    },
    
    --Filter Group
    cutoff_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("filter cutoff cv in")
    },
    resonance_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("filter resonance cv in")
    },
    
    --Mod Group
    intensity_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("mod intensity cv in")
    },
    maxMod_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("maxMod cv in")
    },
    
    --Crush Group
    resolution_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("sample rate cv in")
    },
    depth_cv_in = jbox.cv_input{
        ui_name = jbox.ui_text("bit depth cv in")
    },
    
}

--CV OUTPUTS
cv_outputs = {
    cv_out = jbox.cv_output{
        ui_name = jbox.ui_text("cv output")
    }
}

--AUDIO INPUTS
audio_inputs = {
    L_in = jbox.audio_input{
        ui_name = jbox.ui_text("audio input main left")
    },
    R_in = jbox.audio_input{
        ui_name = jbox.ui_text("audio input main right")
    },
    L_SC = jbox.audio_input{
        ui_name = jbox.ui_text("audio input sidechain left")
    },
    R_SC = jbox.audio_input{
        ui_name = jbox.ui_text("audio input sidechain right")
    }
}

--AUDIO OUTPUTS
audio_outputs = {
    L_out = jbox.audio_output{
        ui_name = jbox.ui_text("audio output main left")
    },
    R_out = jbox.audio_output{
        ui_name = jbox.ui_text("audio output main right")
    }
}


--calls to toolbox functions to configure automatic routing

jbox.add_stereo_audio_routing_pair{
    left = "/audio_inputs/L_in",
    right = "/audio_inputs/R_in"
}

jbox.add_stereo_audio_routing_pair{
    left = "/audio_outputs/L_out",
    right = "/audio_outputs/R_out"
}

jbox.add_stereo_audio_routing_pair{
    left = "/audio_inputs/L_SC",
    right = "/audio_inputs/R_SC"
}

jbox.add_stereo_effect_routing_hint{
    type = "spreading",
    left_input = "/audio_inputs/L_in",
    right_input = "/audio_inputs/R_in",
    left_output = "/audio_outputs/L_out",
    right_output = "/audio_outputs/R_out"
}

jbox.add_stereo_audio_routing_target{
    signal_type = "normal",
    left = "/audio_inputs/L_in",
    right = "/audio_inputs/R_in",
    auto_route_enable = true
}

jbox.add_stereo_audio_routing_target{
    signal_type = "normal",
    left = "/audio_outputs/L_out", 
    right = "/audio_outputs/R_out",
    auto_route_enable = true
}

jbox.set_effect_auto_bypass_routing {
    {"/audio_inputs/L_in", "/audio_outputs/L_out"},
    {"/audio_inputs/R_in", "/audio_outputs/R_out"}
}