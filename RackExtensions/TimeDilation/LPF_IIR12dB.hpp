//
//  LPF_IIR12dB.hpp
//  TimeDilationUnit
//
//  Created by Oscar Alvarado on 2/8/16.
//
//

#ifndef LPF_IIR12dB_hpp
#define LPF_IIR12dB_hpp

#include <stdio.h>

class LPF_IIR12dB{
    
public:
    double streamofs;
    double w;
    double q;
    double r;
    double c;
    double vibrapos;
    double vibraspeed;
    float sampleRate;
    float resFrequency;
    
    LPF_IIR12dB(float sampleRate);
    
    void calculateCoeffs(float cutoffFreq, float amp);
    float processAudioFrames(float input);
    void flushDelays();

};

#endif /* LPF_IIR12dB_hpp */
