//
//  BitCrunch.hpp
//  TimeDilationUnit
//
//  Created by Oscar Alvarado on 1/11/16.
//
//

#ifndef BitCrunch_hpp
#define BitCrunch_hpp

#include <stdio.h>


class BitCruncher
{
public:
    int BitRate;        // for reducing the sample rate
    int BitDepth;        // for quantizing the sample values, for example, to make them 8 bit
    int SampleRate;
    
    BitCruncher(float sampleRate);
    ~BitCruncher(void);
    
    void ProcessSampleBuffer(float * leftSample, float * rightSample, int bufferCount);
};

#endif /* BitCrunch_hpp */
