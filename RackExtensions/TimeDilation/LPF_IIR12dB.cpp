//
//  LPF_IIR12dB.cpp
//  TimeDilationUnit
//
//  Created by Oscar Alvarado on 2/8/16.
//
//

#include "LPF_IIR12dB.hpp"
#include "pluginconstants.h"

LPF_IIR12dB::LPF_IIR12dB(float insampleRate){
    
    this->sampleRate = insampleRate;

}
void LPF_IIR12dB::flushDelays()
{
    streamofs = 0;
    w = 0;
    q = 0;
    r = 0;
    c = 0;
    vibrapos = 0;
    vibraspeed = 0;
}
void LPF_IIR12dB::calculateCoeffs(float cutoffFreq, float amp)
{
    w = 2.0*pi*cutoffFreq/sampleRate; // Pole angle
    q = 1.0-w/(2.0*(amp+0.5/(1.0+w))+w-2.0); // Pole magnitude
    r = q*q;
    c = r+1.0-2.0*cos(w)*q;
    //vibrapos = 0;
    //vibraspeed = 0;

}
float LPF_IIR12dB::processAudioFrames(float input)
{
    /* Main loop */
   // for (streamofs = 0; streamofs < streamsize; streamofs++) {
        
        /* Accelerate vibra by signal-vibra, multiplied by lowpasscutoff */
        vibraspeed += (input - vibrapos) * c;
        
        /* Add velocity to vibra's position */
        vibrapos += vibraspeed;
        
        /* Attenuate/amplify vibra's velocity by resonance */
        vibraspeed *= r;
        
        /* Check clipping */
        float temp = vibrapos;
        if (temp > 32767) {
            temp = 32767;
        } else if (temp < -32768) temp = -32768;
        
        /* Store new value */
        return temp;
}
