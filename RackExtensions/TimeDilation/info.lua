format_version = "1.0"
product_id = "com.moonrocksound.TimeDilation"
version_number = "1.0.0d10"
manufacturer = "Moon Rock Sound"
long_name = "Time Dilation Unit"
medium_name = "Time Dilation"
short_name = "TimeDilatn"
supports_patches = true
default_patch = "/Public/Hair on Your Chest.repatch"
device_type = "creative_fx"
device_height_ru = 2
automation_highlight_color = {r = 60, g = 255, b = 2}
auto_create_track = false
accepts_notes = false
auto_create_note_lane = false
supports_performance_automation = true