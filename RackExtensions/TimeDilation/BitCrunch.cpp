//
//  BitCrunch.cpp
//  TimeDilationUnit
//
//  Created by Oscar Alvarado on 1/11/16.
//
//

#include "BitCrunch.h"

#include <cmath>

BitCruncher::BitCruncher(float sampleRate) :
BitDepth(4),
BitRate(4096),
SampleRate(sampleRate)
{
}


BitCruncher::~BitCruncher(void)
{
}


#define ROUND(f) ((float)((f > 0.0) ? floor(f + 0.5) : ceil(f - 0.5)))

void BitCruncher::ProcessSampleBuffer(float * leftSample, float * rightSample, int bufferCount)
{
    int max = pow(2, BitDepth) - 1;
    int step = SampleRate / BitRate;
    
    int i = 0;
    while (i < bufferCount)
    {
        float leftFirstSample = ROUND((*leftSample + 1.0) * max) / max - 1.0;
        float rightFirstSample = ROUND((*rightSample + 1.0) * max) / max - 1.0;
        
        // this loop causes us to simulate a down-sample to a lower sample rate
        for (int j = 0; j < step && i < bufferCount; j++)
        {
            *leftSample = leftFirstSample;
            *rightSample = rightFirstSample;
            
            // move on
            leftSample++;
            rightSample++;
            i++;
        }
    }
}
