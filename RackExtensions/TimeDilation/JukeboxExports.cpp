#include "CPhaser.h"


void* JBox_Export_CreateNativeObject(const char /*iOperation*/[], const TJBox_Value iParams[], TJBox_UInt32 iCount) {
    JBOX_ASSERT(iCount == 1);
    TJBox_Float64 sampleRate = JBox_GetNumber(iParams[0]);
    int sampleRateInt = static_cast<int>(sampleRate);
    return new CPhaser(sampleRateInt);
}

void JBox_Export_RenderRealtime(void* iPrivateState, const TJBox_PropertyDiff iPropertyDiffs[], TJBox_UInt32 iDiffCount) {
    if(iPrivateState != NULL) {
        CPhaser * effect = reinterpret_cast<CPhaser*>(iPrivateState);
        effect->RenderBatch(iPropertyDiffs, iDiffCount);
    }
}
