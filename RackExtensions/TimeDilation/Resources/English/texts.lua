format_version = "1.0"

 -- english
texts = {
    
					--FRONT PANEL--

--Utilities Group
    ["inputVol"] = "Input Volume",
   		["short inputVol"] = "Input",
		["shortest inputVol"] = "In",
		["ui_type template inputVol"] = "^0 dB",
 
--Pitch Group
	["pitchRange"] = "Pitch Bend Range",
		["short pitchRange"] = "PtchRnge",
		["shortest pitchRange"] = "PRng",
		["0"] = "0",
		["1"] = "1",
		["2"] = "2",
		["3"] = "3",
		["4"] = "4",
		["5"] = "5",
		["6"] = "6",
		["7"] = "7",
		["8"] = "8",
		["9"] = "9",
		["10"] = "10",
		["11"] = "11",
		["12"] = "12",
	["monitor"] = "Monitor",
		["mod on"] = "Mod",
		["mod off"] = "Input",
	["key"] = "Key",
		["main"] = "Main In",
		["sidechain"] = "Sidechain",


--Mod Group
	["intensity"] = "Intensity",
		["ui_type template intensity"] = "^0%",
	["maxMod"] = "MaxMod",
		["ui_type template maxMod"] = "^0%",
	["latency"] = "Latency",
		["ui_type template latency"] = "^0 samples",
	["foldback"] = "Limit/Foldback",
		["foldback on"] = "Foldback",
		["foldback off"] = "Limit",

--Filter Group
	["filterType"] = "Filter Type",
		["hpf"] = "High Pass",
		["lpf"] = "Low Pass",
		["bpf"] = "Band Pass",
	["cutoff"] = "Filter Cutoff",
   		["short cutoff"] = "Cutoff",
		["ui_type template cutoff"] = "^0 Hz",
	["resonance"] = "Filter Resonance",
   		["short resonance"] = "Resonance",
		["ui_type template resonance"] = "^0%",


--Bitcrush Group

	["inCrush"] = "Bitcrush Drive",
   		["short inCrush"] = "Drive",
		["ui_type template inCrush"] = "^0 dB",
	["depthCrush"] = "Bit Depth",
  		["short depthCrush"] = "Bits",
		["ui_type template depthCrush"] = "^0",
	["rateCrush"] = "Sample Rate",
  		["short rateCrush"] = "Rate",
		["ui_type template rateCrush"] = "^0 Hz",
	["pre/post"] = "Filter Pre/Post Bitcrush",
		["pre"] = "———>",
		["post"] = "<———",


--Output Group

 	["mix"] = "Inject",
 		["ui_type template mix"] = "^0%",
    ["outputVol"] = "Output Volume",
   		["short outputVol"] = "Output",
		["shortest outputVol"] = "Out",
		["ui_type template outputVol"] = "^0 dB",
	["spread"] = "Spread On/Off",
		["spread on"] = "On",
		["spread off"] = "Off",


					--BACK PANEL--

--Audio Inputs
	["audio input main left"] = "Left In",
	["audio input main right"] = "Right In",
	["audio input sidechain left"] = "Audio Sidechain Left",
	["audio input sidechain right"] = "Audio Sidechain Right",



--CV Inputs
	["pitch bend range cv in"] = "Pitch Bend Range CV In",
	["pitch bend amt cv in"] = "Pitch Bend Amt CV In",
	["filter cutoff cv in"] = "Filter Cutoff CV In",
	["filter resonance cv in"] = "Filter Resonance CV In",
	["mod intensity cv in"] = "Mod Intensity CV In",
	["maxMod cv in"] = "MaxMod CV In",
	["sample rate cv in"] = "Sample Rate CV In",
	["bit depth cv in"] = "Bit Depth CV In",
	["sidechain cv input"] = "Sidechain CV Input",

--CV Outputs
	["cv output"] = "CV Output",
		   		["short cv output"] = "CV out",

--Audio Outputs
	["audio output main left"] = "Left Out",
	["audio output main right"] = "Right Out",















--SAMPLE PROJECT CODE
  	["propertyname bypass"] = "Bypass",

	["propertyname Volume"] = "Volume",
		["short property name for remote Volume"] = "Volume",
		["shortest property name for remote Volume"] = "Vol",
		["ui_type template Volume"] = "^0 dB",

	["propertyname Load"] = "CPU Load",
		["short property name for remote Load"] = "Load",
		["shortest property name for remote Load"] = "Ld",

	["propertyname DelayTime"] = "Delay Time",
		["short property name for remote DelayTime"] = "DlyTime",
		["shortest property name for remote DelayTime"] = "Dly",

	["propertyname Note On"] = "Note On",
	["propertyname Running"] = "Running",
	["propertyname Waiting for silent timeout"] = "Waiting for silent timeout",
	["propertyname Silent"] = "Silent",
	["propertyname DelaySync"] = "Sync delay time to song",
		["short property name for remote DelaySync"] = "DlySync",
		["shortest property name for remote DelaySync"] = "DSy",
	["propertyname DelayTimeSynced"] = "Sync delay time to note length",
		["short property name for remote DelayTimeSynced"] = "DlyTimeS",
		["shortest property name for remote DelayTimeSynced"] = "Dly",

	["cv input volume"] = "Volume CV",
	["cv output forward"] = "Forward CV",
	["audio input main left"] = "Main In Left",
	["audio input main right"] = "Main In Right",
	["audio output main left"] = "Main Out Left",
	["audio output main right"] = "Main Out Right",
	
	["Unit Template: ms"] = "^0 ms",
	["Unit Template: s"] = "^0 s",
	
	["group name not used"] = "Not used",
}
