/*
	RackAFX(TM)
	Applications Programming Interface
	Derived Class Object Definition
	Copyright(c) Tritone Systems Inc. 2006-2012

	Your plug-in must implement the constructor,
	destructor and virtual Plug-In API Functions below.
*/

#pragma once

// base class
#include "pluginconstants.h"

class CDDLModule
{
public:
	// RackAFX Plug-In API Member Methods:
	// The followung 5 methods must be impelemented for a meaningful Plug-In
	//
	// 1. One Time Initialization
	CDDLModule(int sampleRate);

	// 2. One Time Destruction
    ~CDDLModule(void);

	// 3. The Prepare For Play Function is called just before audio streams
    bool prepareForPlay();

	// 4. processAudioFrame() processes an audio input to create an audio output
    bool  processAudioFrame(float* pInputBuffer, float* pOutputBuffer, UINT uNumInputChannels, UINT uNumOutputChannels);

	// 5. userInterfaceChange() occurs when the user moves a control.
    bool  userInterfaceChange();


	// OPTIONAL ADVANCED METHODS ------------------------------------------------------------------------------------------------
	// These are more advanced; see the website for more details
	//
	// 6. initialize() is called once just after creation; if you need to use Plug-In -> Host methods
	//				   such as sendUpdateGUI(), you must do them here and NOT in the constructor
	bool initialize();
    int m_nSampleRate;
    
	// Add your code here: ----------------------------------------------------------- //
	float m_fDelayInSamples;
	float m_fFeedback;
	float m_fWetLevel;
    float m_fDelay_ms;

	//Delay Line
	float * m_CircularBuffer;
	int m_readIndex;
	int m_writeIndex;
	int m_bufferSize;

	//Post fb insert
	bool m_bUseExternalFeedback; //flag for enable/disable
	float m_fFeedbackIn;		 // user supplied fb value

	//Pre fb insert
	bool m_bUseExternalFeedback2;
	float m_fFeedback2In;
	float m_fExt2out;

	//ext In
	bool m_bExternalInputInUse;
	float m_fExtInput;

	float getCurrentFeedbackOutput(){ return m_fFeedback * m_CircularBuffer[m_readIndex]; }
	void setCurrentFeedbackInput(float f){ m_fFeedback = f; }
	void setUsesExternalFeedback(bool b){ m_bUseExternalFeedback = b; }

	void CookVariables();
	void ResetDelay();
	
	// END OF USER CODE -------------------------------------------------------------- //

};





