WHAT IS THIS?
	This is an example rack extension effect. It's quite simple, but demonstrates a few concepts:
		- a GUI with knobs and lamps and a on/off/bypass button
		- silence handling - i.e. detecting when there is only silence to be rendered and thus avoid writing to the 
		  output buffers to save CPU load.
		- a very simple delay effect with variable delay time. 
		- a GUI button (delay time) with "switched units" and optional tempo sync. You can set it freely in ms or synced to the songs tempo.
		- handling MIDI input and generating an internal sound (even though it is an effect)
		- using CV with CV trim knobs and CV forwarding
		
	The most important part is the silence detection part in order to avoid unnecessary CPU load. To test the device turn
	up the CPU knob a little and feed the effect some audio that goes to silence. You should see the lamps indicating this 
	and that the Recon CPU meter goes down when silence has been detected.

GET STARTED
	Build using XCode or Visual Studio. This builds the project either with LLVM/Clang or the native compiler, depending on 
	configuration/target. Both should work, but you will only be able to debug using the native compiler. The normal build
	scripts (build.py) also installs the rack extension by copying the resulting .lua files, resources and .dll/.dylib to 
	the Rack	Extensions	folder placed in 
		Windows: %APPDATA%\Propellerhead Software\RackExtensions_Dev
		Mac: ~/Library/Application Support/Propellerhead Software/RackExtensions_Dev
		
	If you want to build using command line, use Python 2.7 and check out build.py found in SDK_DIR/Tools/Build.
	
	To get right into the start point where audio is rendered, check out JukeboxExports.cpp/RenderRealtime and 
	SilenceDetectionEffect.h/RenderBatch

CODE PROJECT OVERVIEW
	For this example project the most important class is probably CSilenceDetectionEffect.
	
	There is also GUI "project", found under /GUI, that is to be used with the Rack Extension Designer.
	
	Declaring all possible properties and configuration of the rack extension is done via the lua scripts found under 
	/Scripts. Note that the gui.lua script is generated via the GUI/scripts/hdgui.lua and Rack Extension Designer.

TIPS
	Make sure you build for the right architecture (32/64 bit) - the same as Recon.

	When you're developing your rack extensions it's nice to get a fast workflow. Make sure your build process installs 
	the rack extension in Recon by copying the relevant files. As long as there are no instances of the rack extension in 
	the open song, the .dll/.dylib should be replaceable. By using "Delete devices and tracks" in Recon, then build/install
	and then press "Undo" you can quickly make the library replaceable on disk and still get working in the state you left
	off.
	
	Sometimes when you make script errors in lua, you'll get a popup that asks if you want to retry to parse the file. It 
	may be tempting to correct the script file and press "Retry" but this function is unfortunately not very stable and 
	you may experience weird errors if using this function.

NOTE ON NAMES AND PREFIXES
	We've used the following naming conventions:
		iXXX for input parameter (e.g. iSampleBuffer)
		kXXX for compile time constant value (e.g. kPi = 3.14)
		CXXX for classes (e.g. CVoice)
		EXXX for enums (e.g. EWaveformType)
		declaring public: and private: for every member
		doxygen style comments
	
	

