#include "CPhaser.h"
#include <cmath>
#include "Constants.h"

/**
 ** @brief		Checks if a buffer is completely silent.
 ** @details	Note that everything below kJBox_SilentThreshold should be considered silent.
 **/
static bool IsSilentBuffer(TJBox_AudioSample* iBuffer)
{
    bool silent = true;
    for (size_t i=0; i<kBatchSize; i++) {
        TJBox_AudioSample sampleValue = iBuffer[i];
        if (sampleValue < kJBox_SilentThreshold) {
            sampleValue = -sampleValue;
        }
        if (sampleValue > kJBox_SilentThreshold) {
            silent = false;
            break;
        }
    }
    return silent;
}

enum ESyncedDelayTime {
    k1_8_T,
    k1_8,
    k1_4,
    k1_2
};

enum EFilterType{
    HPF,
    BPF,
    LPF
};


CPhaser::CPhaser(int iSampleRate)
: fVolumeGain(0.0f),
fLastVolumeGain(0.0f),
m_bitRateMin(2048),
m_bitDepthMin(3),
fSampleRate(iSampleRate),
m_DLL_left(iSampleRate),
m_DLL_right(iSampleRate),
m_LPF_12_left(iSampleRate),
m_LPF_12_right(iSampleRate),
m_bitCruncher(iSampleRate),
m_minDly_ms(0),
m_maxDly_ms(50),
fState(kSilent),
fPreviousState(kRunning),
fCheckCount(0),
fFramesSinceInstanceCreation(0),
fCircularBufferPosition(0)
{
    // CPhaserObtain references to the properties we want to use.
    fAudioInLeftObjectRef = JBox_GetMotherboardObjectRef("/audio_inputs/L_in");
    fAudioInRightObjectRef = JBox_GetMotherboardObjectRef("/audio_inputs/R_in");
    fAudioOutLeftObjectRef = JBox_GetMotherboardObjectRef("/audio_outputs/L_out");
    fAudioOutRightObjectRef = JBox_GetMotherboardObjectRef("/audio_outputs/R_out");
    fSideChainInLeftObjectRef = JBox_GetMotherboardObjectRef("/audio_inputs/L_SC");
    fSideChainInRightObjectRef = JBox_GetMotherboardObjectRef("/audio_inputs/R_SC");

    
    fTransportObjectRef = JBox_GetMotherboardObjectRef("/transport");
    
    fCVInObjectRef = JBox_GetMotherboardObjectRef("/cv_inputs/cv_SC");
    fCVOutObjectRef = JBox_GetMotherboardObjectRef("/cv_outputs/cv_out");
    
    fCustomProperties = JBox_GetMotherboardObjectRef("/custom_properties");
    
    //** My Variables ** //
    fInputMeterPropertyRef = JBox_MakePropertyRef(fCustomProperties, "inputVol");
    m_FilterCutoffFreq = JBox_MakePropertyRef(fCustomProperties, "cutoff");
    m_FilterResonance_pct = JBox_MakePropertyRef(fCustomProperties, "resonance");
    m_MonitorOn = JBox_MakePropertyRef(fCustomProperties, "monitor");
    
    
    fLeftAudioBuffer = new TJBox_AudioSample[kBatchSize];
    fRightAudioBuffer = new TJBox_AudioSample[kBatchSize];
    fSidechainBuffer_Left = new TJBox_AudioSample[kBatchSize];
    fSidechainBuffer_Right = new TJBox_AudioSample[kBatchSize];
    ftempBuffer_Left = new TJBox_AudioSample[kBatchSize];
    ftempBuffer_Right = new TJBox_AudioSample[kBatchSize];
    
    fCircularLeftAudioBuffer = new TJBox_AudioSample[kCircularBufferSize];
    fCircularRightAudioBuffer = new TJBox_AudioSample[kCircularBufferSize];
    
    //Bitcrunch
    m_bitCruncher.BitDepth = 24;
    m_bitCruncher.BitRate = 2048;
    
    for (size_t i = 0; i < kBatchSize; i++) {
        fLeftAudioBuffer[i] = 0.0f;
        fRightAudioBuffer[i] = 0.0f;
        fSidechainBuffer_Left[i] = 0.0f;
        fSidechainBuffer_Right[i] = 0.0f;
    }
    
    for (size_t i = 0; i < kCircularBufferSize; i++) {
        fCircularLeftAudioBuffer[i] = 0.0f;
        fCircularRightAudioBuffer[i] = 0.0f;
    }
    
    prepareForPlay();
    
}

bool CPhaser::prepareForPlay()
{
    m_LPF_left.flushDelays();
    m_LPF_right.flushDelays();
    m_LPF_12_left.flushDelays();
    m_LPF_12_right.flushDelays();
    m_DLL_left.prepareForPlay();
    m_DLL_right.prepareForPlay();
    //TODO:: Set the Q anf Cutoff
    
    return true;
}

void CPhaser::UpdateDelayMs(float maxDelay_pct, float leftModSignal, float rightModSignal)
{
    float newDly_ms_left = leftModSignal * (maxDelay_pct * (m_maxDly_ms - m_minDly_ms)) + m_minDly_ms;
    float newDly_ms_right = rightModSignal * (maxDelay_pct * (m_maxDly_ms - m_minDly_ms)) + m_minDly_ms;
    m_DLL_left.m_fDelay_ms = newDly_ms_left;
    m_DLL_right.m_fDelay_ms = newDly_ms_right;
    m_DLL_left.CookVariables();
    m_DLL_right.CookVariables();
}

void CPhaser::calculateHPFCoeffs(float fCutoffFreq, float Q, CBiQuad* pBiQuadFilter)
{
    float theta = (2 * pi * fCutoffFreq) / (float)fSampleRate;
    float d = 1 / Q;
    
    float helpNumber = (d / 2) * sin(theta);
    float Beta = 0.5 * ((1 - helpNumber) / (1 + helpNumber));
    float gamma = (0.5 + Beta) *cos(theta);
    
    float alpha0 = (0.5 + Beta + gamma) / 2.0;
    float alpha1 = (alpha0 * 2) * -1.0 ;
    float alpha2 = alpha0;
    float b1 = -2 * gamma;
    float b2 = 2 * Beta;
    
    pBiQuadFilter->m_f_a0 = alpha0;
    pBiQuadFilter->m_f_a1 = alpha1;
    pBiQuadFilter->m_f_a2 = alpha2;
    pBiQuadFilter->m_f_b1 = b1;
    pBiQuadFilter->m_f_b2 = b2;
}

void CPhaser::calculateLPFCoeffs(float fCutoffFreq, float Q, CBiQuad* pBiQuadFilter)
{
    float theta = (2 * pi * fCutoffFreq) / (float)fSampleRate;
    float d = 1 / Q;
    
    float helpNumber = (d / 2) * sin(theta);
    float Beta = 0.5 * ((1 - helpNumber) / (1 + helpNumber));
    float gamma = (0.5 + Beta) *cos(theta);
    
    float alpha0 = (0.5 + Beta - gamma) / 2.0;
    float alpha1 = alpha0 * 2;
    float alpha2 = alpha0;
    float b1 = -2 * gamma;
    float b2 = 2 * Beta;
    
    pBiQuadFilter->m_f_a0 = alpha0;
    pBiQuadFilter->m_f_a1 = alpha1;
    pBiQuadFilter->m_f_a2 = alpha2;
    pBiQuadFilter->m_f_b1 = b1;
    pBiQuadFilter->m_f_b2 = b2;
}


//Cool Sounding Lowpass With Decibel Measured Resonance
void CPhaser::calculateLPFCoeffsDbMeasuredFilter(float fCutoffFreq, float Q, CBiQuad* pBiQuadFilter)
{
    float c, csq, resonance, q, a0, a1, a2, b1, b2;
    
    c = 1.0f / (tanf(pi * (fCutoffFreq / (float)fSampleRate)));
    csq = c * c;
    resonance = powf(10.0f, -(Q * 0.3f)); //original: resonance = powf(10.0f, -(resonancedB * 0.1f));
    q = sqrt(2.0f) * resonance; // original: q = 0.1 + q * 1.5
    q = 0.5 + q * 0.9;
    a0 = 1.0f / (1.0f + (q * c) + (csq));
    a1 = 2.0f * a0;
    a2 = a0;
    b1 = (2.0f * a0) * (1.0f - csq);
    b2 = a0 * (1.0f - (q * c) + csq);
    
    pBiQuadFilter->m_f_a0 = a0;
    pBiQuadFilter->m_f_a1 = a1;
    pBiQuadFilter->m_f_a2 = a2;
    pBiQuadFilter->m_f_b1 = b1;
    pBiQuadFilter->m_f_b2 = b2;
}

bool CPhaser::processAudioFrames()
{
    //                                  Load CV Status
    TJBox_Float32 cv = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCVInObjectRef, kJBox_CVInputValue));
    TJBox_Bool cvConnected = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fCVInObjectRef, kJBox_CVInputConnected));
    TJBox_Bool keyInSwitchOn = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fCustomProperties, kKeyInSwitchPropertyTag));

    
    //                                  Load SideChain Input
    TJBox_Bool ScLeftConnected;
    TJBox_Bool ScRightConnected;
    if (keyInSwitchOn) {
        ScLeftConnected = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fSideChainInLeftObjectRef, kJBox_AudioInputConnected));
        
        if (ScLeftConnected) {
            
            ScRightConnected = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fSideChainInRightObjectRef, kJBox_AudioInputConnected));
            TJBox_Value leftScIn = JBox_LoadMOMPropertyByTag(fSideChainInLeftObjectRef, kJBox_AudioInputBuffer);
            JBox_GetDSPBufferData(leftScIn, 0, kBatchSize, fSidechainBuffer_Left);
            TJBox_Value rightScIn = JBox_LoadMOMPropertyByTag(fSideChainInRightObjectRef, kJBox_AudioInputBuffer);
            JBox_GetDSPBufferData(rightScIn, 0, kBatchSize, fSidechainBuffer_Right);
        }
    }
    
    bool skipScProcessing = cvConnected && keyInSwitchOn && !ScLeftConnected;
    
    //                                  Load Right Channel Status
    TJBox_Bool rightConnected = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fAudioInRightObjectRef, kJBox_AudioInputConnected));
    
    //                                  Load Monitor Switch Status
    TJBox_Bool monitorOn = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fCustomProperties, kMonitorOnPropertyTag));
    
    //                                  Load MadMox & Intensity Slider
    TJBox_Float32 maxDelay_pct = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kMaxModPropertyTag));
    TJBox_Float32 modIntensity_pct = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kIntensityPropertyTag));
 
    //                                  Load Spread Button Status
    TJBox_Bool spreadOn = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fCustomProperties, kSpreadPropertyTag));

    //                                  Load Foldback Switch
    TJBox_Bool FoldBackOn = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fCustomProperties, kFoldbackSwitchPropertyTag));

    //                                  Load MixKnob
    TJBox_Float32 fmix = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kMixKnobPropertyTag));
    //float fWet_lvl = fWet_lvl_pct;// / 100.0;
    float fDry_lvl, fWet_lvl;
        fmix *= 1000.0;
        float temp = ((321000 + 679 * fmix) * fmix) / 1000000;
        fDry_lvl = 1000000 - temp * temp;
        fDry_lvl /=1000000;
        temp = ((321000 + 679 * (1000.0 - fmix)) * (1000.0 -fmix)) / 1000000;
        fWet_lvl = 1000000 - temp * temp;
        fWet_lvl /=1000000;

    //                                 Prep Output Gain
    TJBox_Float32 vol = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kVolumePropertyTag));
    const float correctedVolume = vol/0.7f; // Correct the OutputVolume value
    fVolumeGain = correctedVolume * correctedVolume * correctedVolume;
    
    //                                  BitCrush
    TJBox_Float32 bitDrive_uncooked = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kBitCrunchDrivePropertyTag));
    float bitDrive = bitDrive_uncooked * 2.0;
    bitDrive = bitDrive * bitDrive * bitDrive;
    TJBox_Int16 bitDepth = static_cast<TJBox_Int16>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kBitDepthPropertyTag));
    TJBox_Int16 bitRate = static_cast<TJBox_Int16>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kBitRatePropertyTag));
    TJBox_UInt16 bitCrunchPost = static_cast<TJBox_UInt16>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kBitCrunchPostPropertyTag));
    
    m_bitCruncher.BitDepth = bitDepth + m_bitDepthMin;
    m_bitCruncher.BitRate = bitRate + m_bitRateMin;
    
    
    //                                Prep Filter Variables
    TJBox_Float32 cutoffFreq_decimal = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kFilterCutoffPropertyTag));
    float correctedCutOff_decimal =  cutoffFreq_decimal * cutoffFreq_decimal * cutoffFreq_decimal;
    int cutoffFreq = (correctedCutOff_decimal) * (16000 - 32) + 32;
    TJBox_Float32 resonance = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kFilterResonancePropertyTag));
    TJBox_Int16 filterType = static_cast<TJBox_Int16>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kFilterTypePropertyTag));
    
    float MaxQ = 50, MinQ = 0.8;
    float Q_Range = MaxQ - MinQ; // Max - Min
    float Q = resonance * Q_Range + MinQ;
    
    if (filterType == EFilterType::LPF) {
        calculateLPFCoeffs(cutoffFreq, Q, &m_LPF_left);
        calculateLPFCoeffs(cutoffFreq, Q, &m_LPF_right);
    }
    else if (filterType == EFilterType::BPF) {
        //TODO
//        calculateLPFCoeffsDbMeasuredFilter(cutoffFreq, Q, &m_LPF_left);
//        calculateLPFCoeffsDbMeasuredFilter(cutoffFreq, Q, & m_LPF_right);
        m_LPF_12_left.calculateCoeffs(cutoffFreq, Q <= 0? 0.001f : Q);
        m_LPF_12_right.calculateCoeffs(cutoffFreq, Q <= 0? 0.001f : Q);
    }
    else if (filterType == EFilterType::HPF) {
        calculateHPFCoeffs(cutoffFreq, Q, &m_LPF_left);
        calculateHPFCoeffs(cutoffFreq, Q, &m_LPF_right);
    }
    
    
    // **** PROCESSING LOOP **** //
    float fLeftIn, fRightIn, fLeftOut, fRightOut, fLeftScOut, fRightScOut, fLeftModIn, fRightModIn;
    
    float lef, rig;
    
    for (size_t i = 0; i < kBatchSize; i++) {
    
 
        // Check For Sidechain and Assign ModLine input
        // If Switch is Off OR if either SC or CvIn are not connected -> copy input to modSignal
        if(!keyInSwitchOn || (!ScLeftConnected && !cvConnected) ){
            
            fLeftModIn =  fLeftAudioBuffer[i];
            fRightModIn = rightConnected ? fRightAudioBuffer[i] : fLeftModIn;
        
        } else if(ScLeftConnected)
        {
            fLeftModIn = fSidechainBuffer_Left[i];
            
            // Copy the left input if mono signal
            if (ScRightConnected)
                fRightModIn = fSidechainBuffer_Right[i];
            else
                fRightModIn = fLeftModIn;
            
        }
        
        if (!skipScProcessing)
        {
            if(bitCrunchPost == false)
            {
                //Bitcrunch
                fLeftScOut = fLeftModIn * bitDrive;
                fRightScOut = fRightModIn * bitDrive;
                
                ftempBuffer_Left[i] = fLeftScOut;
                ftempBuffer_Right[i] = fRightScOut;
            }
            else {
                //Frequency Filter
                if (filterType == EFilterType::BPF)
                {
                    fLeftScOut = m_LPF_12_left.processAudioFrames(fLeftModIn);
                    fRightScOut = m_LPF_12_right.processAudioFrames(fRightModIn);
                }else {
                    fLeftScOut = m_LPF_left.doBiQuad(fLeftModIn);
                    fRightScOut = m_LPF_right.doBiQuad(fRightModIn);
                }
                //Bitcrunch
                fLeftScOut *= bitDrive;
                fRightScOut *= bitDrive;
                
                ftempBuffer_Left[i] = fLeftScOut;
                ftempBuffer_Right[i] = fRightScOut;

            }
        }
        
    }
    
    if (!skipScProcessing)
        m_bitCruncher.ProcessSampleBuffer(ftempBuffer_Left, ftempBuffer_Right, kBatchSize);

    for (size_t t = 0; t < kBatchSize; t++) {

        fLeftOut =  fLeftIn = fLeftAudioBuffer[t];
        // Copy the left input if mono signal
        if (rightConnected)
            fRightIn = fRightAudioBuffer[t];
        else
            fRightIn = fLeftIn;
        
        //..Dsp..
        if (!skipScProcessing)
        {
            if(bitCrunchPost == false)
            {
                //Frequency Filter
                if (filterType == BPF)
                {
                    fLeftScOut = m_LPF_12_left.processAudioFrames(ftempBuffer_Left[t]);
                    fRightScOut =m_LPF_12_right.processAudioFrames(ftempBuffer_Right[t]);
                }
                else
                {
                    fLeftScOut = m_LPF_left.doBiQuad(ftempBuffer_Left[t]);
                    fRightScOut = m_LPF_right.doBiQuad(ftempBuffer_Right[t]);
                }
            } else {
                fLeftScOut = ftempBuffer_Left[t];
                fRightScOut = ftempBuffer_Right[t];
            }
            
//            // if Spead is On, Invert Right Mod Signal
//            if (spreadOn) {
//                fRightScOut *= -1.0;
//            }
            
            
            if (ScLeftConnected && cvConnected && keyInSwitchOn) {
                fLeftScOut *= cv;
                fRightScOut *= cv;
            }
        }
        else
        {
            fLeftScOut = cv;
            fRightScOut = cv;
        }
        
        // if Spead is On, Invert Right Mod Signal
        if (spreadOn) {
            fRightScOut *= -1.0;
        }
        
        // Calculate Delay in ms based on SC Buffer
        float modSignalScaled_left = fLeftScOut * modIntensity_pct;
        float modSignalScaler_right = fRightScOut * modIntensity_pct;
        UpdateDelayMs(maxDelay_pct, modSignalScaled_left, modSignalScaler_right);
        
        // Run the Delays
        float delayOut_left, delayOut_right;
        m_DLL_left.processAudioFrame(&fLeftIn, &delayOut_left, 1, 1);
        m_DLL_right.processAudioFrame(&fRightIn, &delayOut_right, 1, 1);
        
        // Sum Outputs
        if (!monitorOn)
        {
            fLeftOut = (fLeftIn * fDry_lvl) + (delayOut_left * fWet_lvl);
            fRightOut= (fRightIn * fDry_lvl) +(delayOut_right * fWet_lvl);
        } else {
            // If monitor is on, Mix output with SC Buffer
            fLeftOut = (fLeftIn * fDry_lvl) + (fLeftScOut * fWet_lvl);
            fRightOut = (fRightIn * fDry_lvl) + (fRightScOut * fWet_lvl);
        }
        
        // Apply Output Gain
        lef =  fLeftOut  * fVolumeGain;
        rig = fRightOut * fVolumeGain;
        fLeftAudioBuffer[t] =  lef;
        fRightAudioBuffer[t] = rig;
        
    }
    
   // m_bitCruncher.ProcessSampleBuffer(fLeftAudioBuffer, fRightAudioBuffer, 64);
    
    JBox_StoreMOMPropertyAsNumber(fCVOutObjectRef, kJBox_CVOutputValue, cv);
    
    return true;
}

void CPhaser::RenderBatch(const TJBox_PropertyDiff iPropertyDiffs[], TJBox_UInt32 iDiffCount)
{
    // Read Bypass Switch
    TJBox_Int32 onoffbypass = static_cast<TJBox_Int32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kJBox_CustomPropertiesOnOffBypass));
    
    // Read Monitor Switch
//    TJBox_Bool monitorOn = JBox_GetBoolean(JBox_LoadMOMPropertyByTag(fCustomProperties, kMonitorOnPropertyTag));
    TJBox_Value leftAudioInValue = JBox_LoadMOMPropertyByTag(fAudioInLeftObjectRef, kJBox_AudioInputBuffer);
    JBox_GetDSPBufferData(leftAudioInValue, 0, kBatchSize, fLeftAudioBuffer);
    TJBox_Value rightAudioInValue = JBox_LoadMOMPropertyByTag(fAudioInRightObjectRef, kJBox_AudioInputBuffer);
    JBox_GetDSPBufferData(rightAudioInValue, 0, kBatchSize, fRightAudioBuffer);
    
//    TJBox_Value leftScIn = JBox_LoadMOMPropertyByTag(fSideChainInLeftObjectRef, kJBox_AudioInputBuffer);
//    JBox_GetDSPBufferData(leftScIn, 0, kBatchSize, fSidechainBuffer_Left);
//    TJBox_Value rightScIn = JBox_LoadMOMPropertyByTag(fSideChainInRightObjectRef, kJBox_AudioInputBuffer);
//    JBox_GetDSPBufferData(rightScIn, 0, kBatchSize, fSidechainBuffer_Right);    
    
    // If Rack is not enabled, Skip processing code
    if (onoffbypass == kJBox_EnabledOff) {
        fState = kSilent;
        UpdateInputMeter();
    }
    else if (onoffbypass == kJBox_EnabledBypass){
        
        for (size_t i = 0; i < kBatchSize; i++) {
            
            float fLeftIn = fLeftAudioBuffer[i];
            float fRightIn = fRightAudioBuffer[i];
            
            fLeftIn = m_LPF_left.doBiQuad(fLeftIn);
            fRightIn = m_LPF_right.doBiQuad(fRightIn);
            
            float delayOut_left, delayOut_right;
            m_DLL_left.processAudioFrame(&fLeftIn, &delayOut_left, 1, 1);
            m_DLL_right.processAudioFrame(&fRightIn, &delayOut_right, 1, 1);
        }
    }
    else {
        
        // *** Read audio inputs ***** //
        
       
        
        
        // *** Check For Silent -> Running ***** //
        
        if (fState == kSilent) {
            if (!IsSilentBuffer(fLeftAudioBuffer) || !IsSilentBuffer(fRightAudioBuffer)) {
                fState = kRunning;
                TransitionFromSilentToRunning();
            }
        }
        
        if (fCheckCount++ % 10 == 0)
            UpdateInputMeter();
        
        
        // *** DO PROCESSING ***** //
        
        if (fState == kRunning) {
            
            if (onoffbypass == kJBox_EnabledOn) {  // check for Running -> Silent change
                if (fCheckCount % 100 == 0) {
                    if (IsSilentBuffer(fLeftAudioBuffer) && IsSilentBuffer(fRightAudioBuffer)) {
                        fState = kSilent;
                        prepareForPlay();
                    }
                }
                
                // ***** DSP Code ****** //
                processAudioFrames();
            }
            
            // **** Bypass Switch is On ***** //
            else {
                JBOX_ASSERT(onoffbypass == kJBox_EnabledBypass);
                if (fState == kRunning) {
                    // Check inputs for silence, but not so often.
                    if (fCheckCount % 100 == 0) {
                        bool inputsAreSilent = IsSilentBuffer(fLeftAudioBuffer) && IsSilentBuffer(fRightAudioBuffer);
                        if (inputsAreSilent) {
                            fState = kSilent;
                            // Write modified audio buffers to outputs.
                            JBOX_TRACE("Setting to silent");
                        }
                    }
                }
                else {
                    // kSilence
                    if (!IsSilentBuffer(fLeftAudioBuffer) || !IsSilentBuffer(fRightAudioBuffer)) {
                        fState = kRunning;
                        TransitionFromSilentToRunning();
                    }
                }
            }
        }
    }
    
    // ***** Write To Output Buffers ***** //
    
    if (fState != kSilent) {
        
            TJBox_Value leftAudioOutValue = JBox_LoadMOMPropertyByTag(fAudioOutLeftObjectRef, kJBox_AudioOutputBuffer);
            JBox_SetDSPBufferData(leftAudioOutValue, 0, kBatchSize, fLeftAudioBuffer);
            
            TJBox_Value rightAudioOutValue = JBox_LoadMOMPropertyByTag(fAudioOutRightObjectRef, kJBox_AudioOutputBuffer);
            JBox_SetDSPBufferData(rightAudioOutValue, 0, kBatchSize, fRightAudioBuffer);
    }
        
        
    if (fPreviousState != fState) {
        fPreviousState = fState;
    }
        
    fFramesSinceInstanceCreation += kBatchSize;
}

    
void CPhaser::UpdateInputMeter()
{
        TJBox_Float64 avgVol = 0.0f;
        //TJBox_Float64 maxVol = -1.0;
        TJBox_Float64 correctedMeterVal = 0.0f;
        if (fState != kSilent)
        {
            // Get an average from all the samples.
            for (size_t i = 0; i < kBatchSize; i += 8)
            {
                avgVol += fLeftAudioBuffer[i] + fRightAudioBuffer[i];
            }
            
            avgVol /= 2;
            avgVol /= kBatchSize;
            
            //    Linear conversion. (data_value) * (10 - 0.5) + 0.5
            //                  0 - 1 => -30 -> 30
            //                      gui * 60 - 30
            
            //
            //    new_value = ( (old_value - old_min) / (old_max - old_min) ) * (new_max - new_min) + new_min
            //    new_value = ( (avgVol    - (-1.0 )) / (1.0     - (-1.0) ) ) * (1.0     - 0.0    ) + 0.0f;
            correctedMeterVal = ((avgVol + 1.0) / 2.0);
        }
        else
            correctedMeterVal = 0;
        
        // Write Value to the input meter GUI
        JBox_StoreMOMPropertyAsNumber(fCustomProperties, JBox_GetPropertyTag(fInputMeterPropertyRef), correctedMeterVal);
        
}

    
void CPhaser::VolumeProcessing()
{
        // Get CV Status
        TJBox_Float32 cv = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCVInObjectRef, kJBox_CVInputValue));
        TJBox_Value cvConnectedValue = JBox_LoadMOMPropertyByTag(fCVInObjectRef, kJBox_CVInputConnected);
        TJBox_Bool cvConnected = JBox_GetBoolean(cvConnectedValue);
        
        // Get value of volume property [0.0 .. 1.0]
        TJBox_Float32 vol = static_cast<TJBox_Float32>(JBox_LoadMOMPropertyAsNumber(fCustomProperties, kVolumePropertyTag));
        if (cvConnected) {
            vol *= cv;
        }
        
        // Correct the volume value
        const float correctedVolume = vol/0.7f;
        float targetGain = correctedVolume * correctedVolume * correctedVolume;
        
        fLastVolumeGain = fVolumeGain;
        fVolumeGain  = targetGain;
        
        // Multiply audio buffers by value.
        for (size_t i = 0; i < kBatchSize; i++) {
            fLeftAudioBuffer[i] *= fVolumeGain;
            fRightAudioBuffer[i] *= fVolumeGain;
        }
        
        JBox_StoreMOMPropertyAsNumber(fCVOutObjectRef, kJBox_CVOutputValue, cv);
}

void CPhaser::TransitionFromSilentToRunning()
{
        for (size_t i = 0; i < kCircularBufferSize; i++) {
            fCircularLeftAudioBuffer[i] = 0.0f;
            fCircularRightAudioBuffer[i] = 0.0f;
        }
        for (size_t i = 0; i < kBatchSize; i++) {
            fSidechainBuffer_Left[i] = 0.0f;
            fSidechainBuffer_Right[i] = 0.0f;
        }
        
    prepareForPlay();
        //TODO:: Set the Q anf Cutoff
}

