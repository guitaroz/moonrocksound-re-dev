/*
	RackAFX(TM)
	Applications Programming Interface
	Derived Class Object Implementation
*/


#include "DDLModule.h"


/* constructor()
	You can initialize variables here.
	You can also allocate memory here as long is it does not
	require the plugin to be fully instantiated. If so, allocate in init()

*/
CDDLModule::CDDLModule(int sampleRate)
{
	// Added by RackAFX - DO NOT REMOVE
	

	// Finish initializations here
	m_fDelayInSamples = 0;
	m_fDelay_ms = 0;
    m_fDelay_ms = 0;
	m_fFeedback = 0;
	m_fWetLevel = 0;
    m_nSampleRate = sampleRate;
	m_bUseExternalFeedback = false;
	m_bUseExternalFeedback2 = false;
	m_bExternalInputInUse = false;
	m_fExt2out = 0.0;
	m_fFeedback2In = 0.0;
	m_fExtInput = 0.0;

	m_readIndex = 0;
	m_writeIndex = 0;
	m_bufferSize = 0;
	m_CircularBuffer = NULL;

	CookVariables();
}

void CDDLModule::CookVariables()
{
//	m_fFeedback = m_f_Feedback_pct / 100.0;
//	m_fWetLevel = m_f_WetLevel_pct / 100.0;
    m_fFeedback = 0.0;
    m_fWetLevel = 1.0;
	m_fDelayInSamples = m_fDelay_ms * ((float)m_nSampleRate / 1000.0);

	m_readIndex = m_writeIndex - (int)m_fDelayInSamples; //cast!

	if (m_readIndex < 0)
		m_readIndex += m_bufferSize; //wrap around = Read + Length
    else if (m_readIndex > m_bufferSize)
        m_readIndex = m_readIndex - m_bufferSize;

}
/* destructor()
	Destroy variables allocated in the contructor()

*/
CDDLModule::~CDDLModule(void)
{

	if (m_CircularBuffer)
		delete[] m_CircularBuffer;
}




/* prepareForPlay()
	Called by the client after Play() is initiated but before audio streams

	You can perform buffer flushes and per-run intializations.
	You can check the following variables and use them if needed:

	m_nNumWAVEChannels;
	m_nSampleRate;
	m_nBitDepth;

	NOTE: the above values are only valid during prepareForPlay() and
		  processAudioFrame() because the user might change to another wave file,
		  or use the sound card, oscillators, or impulse response mechanisms

    NOTE: if you allocte memory in this function, destroy it in ::destroy() above
*/
bool CDDLModule::prepareForPlay()
{
	m_bufferSize = 2 * m_nSampleRate; //2 seconds

	if (m_CircularBuffer)
		delete[] m_CircularBuffer;

	m_CircularBuffer = new float[m_bufferSize];

	ResetDelay();
	CookVariables();
	return true;
}


/* processAudioFrame

// ALL VALUES IN AND OUT ON THE RANGE OF -1.0 TO + 1.0

LEFT INPUT = pInputBuffer[0];
RIGHT INPUT = pInputBuffer[1]

LEFT OUTPUT = pInputBuffer[0]
RIGHT OUTPUT = pOutputBuffer[1]

*/
bool CDDLModule::processAudioFrame(float* pInputBuffer, float* pOutputBuffer, UINT uNumInputChannels, UINT uNumOutputChannels)
{

	// Yn = dry * Xn + wet * (X(n-D) + fb*y(n-D);
	float xn;
	float yn = m_CircularBuffer[m_readIndex];
	if (m_bExternalInputInUse)
		xn = m_fExtInput;
	else
		xn = pInputBuffer[0];

	//if delay < 1 sample, interpolate betwenn x(n) and x(n-1)
	if (m_readIndex == m_writeIndex && m_fDelayInSamples < 1.00)
		yn = xn;

	int readIndex_1 = m_readIndex - 1;
	if (readIndex_1 < 0)
		readIndex_1 = m_bufferSize - 1;

	float y_1 = m_CircularBuffer[readIndex_1];

	//interpolate (0,yn) (1,yn_1) by fracDelay
	float fracDelay = m_fDelayInSamples - (int)m_fDelayInSamples;

	float fInterp = dLinTerp(0, 1, yn, y_1, fracDelay);

	//if delay is zero just pass input to yn
	if (m_fDelayInSamples == 0)
		yn = xn;
	else
		yn = fInterp;

	// Update fx2 out
	m_fExt2out = yn;

	float Yn_final;

	if (!m_bUseExternalFeedback2)
		Yn_final = yn;
	else
		Yn_final = m_fFeedback2In;

	//Post feedback switch
	if (!m_bUseExternalFeedback)
		m_CircularBuffer[m_writeIndex] = xn + m_fFeedback *Yn_final;  //normfInterpal
	else
		m_CircularBuffer[m_writeIndex] = xn + m_fFeedbackIn;	//external feedback



	//write out to buffer
	pOutputBuffer[0] = m_fWetLevel * yn + (1.0 - m_fWetLevel) * xn;
	
	//increase index and check for wrap
	m_readIndex++;
	if (m_readIndex >= m_bufferSize)
		m_readIndex = 0;

	m_writeIndex++;
	if (m_writeIndex >= m_bufferSize)
		m_writeIndex = 0;

	// Mono-In, Stereo-Out (AUX Effect)
	if(uNumInputChannels == 1 && uNumOutputChannels == 2)
		pOutputBuffer[1] = pOutputBuffer[0]; //Copy mono
	
	//DLL Module is Mono. copy to mono here as well.
	// Stereo-In, Stereo-Out (INSERT Effect)
	if(uNumInputChannels == 2 && uNumOutputChannels == 2)
		pOutputBuffer[1] = pOutputBuffer[0];


	return true;
}

//flush buffer and set write pointer back to top. read pointer will be calculated based on write pointer
void CDDLModule::ResetDelay()
{

	if (m_CircularBuffer)
		memset(m_CircularBuffer, 0, m_bufferSize * sizeof(float));

	m_writeIndex = 0;
	m_readIndex = 0;

}

bool CDDLModule::userInterfaceChange()
{
	CookVariables();
	return true;
}


