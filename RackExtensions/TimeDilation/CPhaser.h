#pragma once
#ifndef PHASER_H
#define PHASER_H

#include <cstdlib>
#include "pluginconstants.h"
#include "Jukebox.h"
#include "DDLModule.h"
#include "BitCrunch.h"
#include "LPF_IIR12dB.hpp"


/**
 ** @brief	This is the main class for the project/effect and where most things happen.
 ** @details This effect device takes incoming audio, or generates audio from MIDI, runs it through a simple delay
 **		effect and then tries to detect if only silence is produced. If so, it shortcuts the processing in order
 **		to save CPU power. To illustrate this concept further, there is also some "Dummy processing" taking place
 **		when the device is active (see the Processing function). The basic principle is keep a state of if it's
 **		silent or running. If it's running, we check if silence is produced and if so change the state. Note that
 **		we don't check every Render call in order to save some CPU power.
 **/
class CPhaser
{
    /**
     States for the 45 silence handling.
     
     kRunning: There are sound on the inputs and/or the outputs and the 45 is rendering.
     The 45 are rendering and checking the output of the algorithm to become silent.
     If output of the algorithm become silent, the kWaitingForSilentTimeout state is entered.
     kWaitingForSilentTimeout: Input sockets and output from the algorithm are checked every batch.
     If any of them becomes non silent or a any note is pressed the state is switched to kRunning.
     After a specific time the kSilent state is entered.
     kSilent: Input sockets and output from the algorithm are silent. The 45 is not executing its rendering code.
     It may have to increase a frame counter to keep track of the amount of frames played during this state to
     be able to catch up (chase) when entering the kRunning state again. If sound comes on inputs during this state
     or if a note is pressed, the state is switched to kRunning.
     */
public: enum EState {
    kSilent = 0,
    kWaitingForSilentTimeout,
    kRunning
};
    
public: CPhaser(int iSampleRate);
    
    // This class ought to have a destructor that deletes fLeft/RightAudioBuffer and fCircularLeft/RightAudioBuffer,
    //	but the destructor for objects returned by JBox_Export_CreateNativeObject is never called,
    //	and all memory we allocate is automatically deallocated by the host, so we can get away with just letting it be.
    // public: ~CSilenceDetectionEffect();
public: void RenderBatch(const TJBox_PropertyDiff iPropertyDiffs[], TJBox_UInt32 iDiffCount);
    
    // ******* PHASER VARIABLES ******** //
    
private: TJBox_PropertyRef m_FilterCutoffFreq;
private: TJBox_PropertyRef m_FilterResonance_pct;
private: TJBox_PropertyRef m_MonitorOn;
    
private: TJBox_AudioSample* fLeftAudioBuffer;
private: TJBox_AudioSample* fRightAudioBuffer;
private: TJBox_AudioSample* fSidechainBuffer_Left;
private: TJBox_AudioSample* fSidechainBuffer_Right;
    
private: float* ftempBuffer_Left;
private: float* ftempBuffer_Right;
    
    
    CBiQuad m_LPF_left;
    CBiQuad m_LPF_right;
    LPF_IIR12dB m_LPF_12_left;
    LPF_IIR12dB m_LPF_12_right;
    
    
    CDDLModule m_DLL_left;
    CDDLModule m_DLL_right;
    float m_minDly_ms;
    float m_maxDly_ms;
    BitCruncher m_bitCruncher;
    
    void calculateLPFCoeffs(float fCutoffFreq, float Q, CBiQuad* pBiQuadFilter);
    void calculateHPFCoeffs(float fCutoffFreq, float Q, CBiQuad* pBiQuadFilter);
    void calculateLPFCoeffsDbMeasuredFilter(float fCutoffFreq, float Q, CBiQuad* pBiQuadFilter);

    bool prepareForPlay();
    bool processAudioFrames();
    void UpdateInputMeter();
    void UpdateDelayMs(float maxDly_pct,float leftModSignal, float rightModSignal);
    
    float m_bitRateMin;
    float m_bitDepthMin;
    
    // ******* END PHASER VARIABLES ******** //
    
    // Privates:
private: bool CheckForNoteOn(TJBox_UInt32 iDiffCount, const TJBox_PropertyDiff * iPropertyDiffs);
private: void Processing(TJBox_Int64& oSilentTimeoutFrames);
private: void VolumeProcessing();
private: void TransitionFromSilentToRunning();
    
private: TJBox_ObjectRef fAudioInLeftObjectRef;
private: TJBox_ObjectRef fAudioInRightObjectRef;
private: TJBox_ObjectRef fAudioOutLeftObjectRef;
private: TJBox_ObjectRef fAudioOutRightObjectRef;
private: TJBox_ObjectRef fSideChainInLeftObjectRef;
private: TJBox_ObjectRef fSideChainInRightObjectRef;
private: TJBox_ObjectRef fCVInObjectRef;
private: TJBox_ObjectRef fCVOutObjectRef;
private: TJBox_ObjectRef fTransportObjectRef;
private: TJBox_ObjectRef fCustomProperties;
    
private: TJBox_PropertyRef fInputMeterPropertyRef;
private: TJBox_PropertyRef fRunningPropertyRef;
private: TJBox_PropertyRef fWaitingForSilentTimeoutPropertyRef;
private: TJBox_PropertyRef fSilentPropertyRef;
    
private: TJBox_Float32 fVolumeGain;
private: TJBox_Float32 fLastVolumeGain;
private: int fSampleRate;
private: EState fState;
private: EState fPreviousState;
private: int fCheckCount;
private: TJBox_Int64 fFramesUntilSilent;
    
private: TJBox_Int64 fFramesSinceInstanceCreation;
    
private: TJBox_AudioSample* fCircularLeftAudioBuffer;
private: TJBox_AudioSample* fCircularRightAudioBuffer;
private: size_t fCircularBufferPosition;
};

#endif
